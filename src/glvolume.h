/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __glvolume_h
#define __glvolume_h
#include <gtk/gtk.h>

#include "gldrawable.h"
#include "image3d.h"

#define MIA_TYPE_GLVOLUME (mia_glvolume_get_type())
#define MIA_GLVOLUME(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIA_TYPE_GLVOLUME, MiaGLVolume))
#define MIA_GLVOLUME_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIA_TYPE_GLVOLUME, MiaGLVolumeClass))
#define MIA_IS_GLVOLUME(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIA_TYPE_GLVOLUME))
#define MIA_IS_GLVOLUME_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIA_TYPE_GLVOLUME))
#define MIA_GLVOLUME_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst), MIA_TYPE_GLVOLUME ,MiaGLVolumeClass))

typedef struct _MiaGLVolume MiaGLVolume;
typedef struct _MiaGLVolumeClass MiaGLVolumeClass;

struct _MiaGLVolumeClass {
	MiaGLDrawableClass parent;
};

GType mia_glvolume_get_type(void);

MiaGLVolume *mia_glvolume_new(const gchar *name, GtkRange *iso_range_min, GtkRange *iso_range_max);

MiaVector3d *mia_glvolume_set_volume(MiaGLVolume *self, MiaImage3d *image);

#endif

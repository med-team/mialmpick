/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "globals.h"
#include <gtk/gtk.h>
#include <stdarg.h>


GtkWidget *mainwindow;
GtkWidget *glview; 
GladeXML *xml;
float iso_value_max = 256; 
float iso_value_min = 128; 
GtkRange *range_iso_value_min = NULL; 
GtkRange *range_iso_value_max = NULL; 

const char *in_filename = 0; 
const char *lm_filename = 0; 
const char *out_filename = 0; 

int snap_number = 0; 

void global_show_error_message(const gchar *format, ...)
{
	va_list args;
	GtkWidget *dialog;
	va_start(args, format);
	dialog = gtk_message_dialog_new(GTK_WINDOW(mainwindow), 
				GTK_DIALOG_DESTROY_WITH_PARENT,
			        GTK_MESSAGE_ERROR,
                                GTK_BUTTONS_CLOSE,
			        format,args);
	va_end (args);
	
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

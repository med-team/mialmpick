/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#define GL_GLEXT_PROTOTYPES 1

#include <GL/gl.h>
#include <GL/glu.h>
//#include <Cg/cg.h>
//#include <Cg/cgGL.h>
#include <math.h>

#include "glvolume.h"
#include "globals.h"



struct _MiaGLVolume {
	MiaGLDrawable parent;
	gboolean dispose_has_run;
	/* add your class instance elements here */
	GLuint volume_tex;
	GLfloat iso_value;

	/* CG stuff */
	GLhandleARB cg_vtxprog;
	GLhandleARB cg_fragprog;
	GLhandleARB shader_program; 

	
	GLint cg_light_direction_f3;
	GLint cg_delta_f3;
	GLint cg_scale_f3;
	GLint cg_alpha_discard_min; 
	GLint cg_alpha_discard_max; 

	MiaVector3d *light_direction;
	GtkRange *iso_range_max;
	GtkRange *iso_range_min;
	gfloat iso_max; 
	gint nx, ny, nz;
	gfloat sx, sy, sz; 
	gfloat *pixels;

	GMutex pixel_mutex; 
};

static GObject *__mia_glvolume_constructor (GType type,
					    guint n_construct_properties,
					    GObjectConstructParam *
					    construct_properties);


static void
  __mia_glvolume_class_init (gpointer g_class, gpointer g_class_data);

static void
  __mia_glvolume_instance_init (GTypeInstance * instance, gpointer g_class);

static void __gl_attach (MiaGLDrawable * self);
static void __gl_detach (MiaGLDrawable * self);
static void __gl_draw (MiaGLDrawable * self, MiauiGLView * view,
		       gboolean high_resolution);
static void 
__initialize(MiaGLDrawable *obj);

GType
mia_glvolume_get_type (void)
{
	static GType type = 0;
	if (type == 0) {
		static const GTypeInfo info = {
			sizeof (MiaGLVolumeClass),
			NULL,	/* base_init */
			NULL,	/* base_finalize */
			__mia_glvolume_class_init,	/* class_init */
			NULL,	/* class_finalize */
			NULL,	/* class_data */
			sizeof (MiaGLVolume),
			0,	/* n_preallocs */
			__mia_glvolume_instance_init,	/* instance_init */
			NULL
		};
		type = g_type_register_static (MIA_TYPE_GLDRAWABLE,
					       "MiaGLVolumeType", &info, 0);
	}
	return type;
}




MiaGLVolume *
mia_glvolume_new (const gchar *name, GtkRange *iso_range_min, GtkRange *iso_range_max)
{
	MiaGLVolume *obj =
		(MiaGLVolume *) g_object_new (MIA_TYPE_GLVOLUME, 
			mia_gldrawable_name_property, name, NULL);
	/* add your code here */
	obj->light_direction = NULL;
	obj->iso_range_max = iso_range_max; 
	obj->iso_range_min = iso_range_min; 
	obj->iso_max = 0.0; 
	g_object_ref(iso_range_max);
	g_object_ref(iso_range_min);
	obj->sx = obj->sy = obj->sz = 1.0; 
	return obj;
}

extern int max_texture_size; 

gboolean is_pow2_below(unsigned int value, unsigned int max) 
{
	if (value >= max) 
		return FALSE; 
	unsigned int t = 1; 
	unsigned int v = value;  
	while (v > 1) {
		t <<= 1; 
		v >>=1; 
	}
	return value == t; 
}

MiaVector3d *mia_glvolume_set_volume(MiaGLVolume *self, MiaImage3d *image)
{
	MiaImage3d *pending = NULL; 
	float *p, *pixels;
	gfloat mini;
	gfloat maxi; 
	gfloat *img; 
	
	gfloat alpha_scale; 
	unsigned int x,y,z; 

	gfloat sx, sy, sz; 
	gfloat sum;

	pending = image; 
	/*pending  = mia_image3d_crop_pow2(image, max_texture_size);*/

	/* reuse possibly allocated image */
	g_mutex_lock(&self->pixel_mutex);
	pixels = self->pixels; 
	self->pixels = NULL; 
	g_mutex_unlock(&self->pixel_mutex);

	
	if (!pixels)
		pixels = g_malloc0(pending->size * sizeof(gfloat)); 
	
	if (!pixels) {
		g_warning("not enough memory to create intermeadiate texture");
		return NULL; 
	}
	
	
	p = pixels;
	
	mia_image3d_get_minmax(pending, &mini, &maxi);
	self->iso_max = maxi; 
	
	alpha_scale = 1.0 / self->iso_max;

	gtk_range_set_range(self->iso_range_max, mini, self->iso_max);
	gtk_range_set_range(self->iso_range_min, mini, self->iso_max);
	gtk_range_set_value(self->iso_range_max, self->iso_max);
	gtk_range_set_value(self->iso_range_min, self->iso_max * 0.5);
	
	sum = 0.0; 
	sx = sy = sz = 0.0;

	
	img = pending->data;
	for (z = 0; z < pending->nz; ++z)
		for (y = 0; y < pending->ny; ++y) 
			for (x = 0; x < pending->nx; ++x, ++p, ++img) {
				*p = *img * alpha_scale;
				sx += *p * x; 
				sy += *p * y; 
				sz += *p * z; 
				sum += *p; 
				
			}
	sum *=128; 

	self->nx = pending->nx;
	self->ny = pending->ny;
	self->nz = pending->nz;

	self->sx = image->sx * self->nx / 256.0; 
	self->sy = image->sy * self->ny / 256.0; 
	self->sz = image->sz * self->nz / 256.0;

	g_object_unref(pending);
	
	/* Data copied, make it available */
	g_mutex_lock(&self->pixel_mutex);
	self->pixels = pixels; 
	g_mutex_unlock(&self->pixel_mutex);
	
	g_message("Center @ %f %f %f", self->sx, self->sy, self->sz);
	return mia_vector3d_new(self->sx, self->sy, self->sz); 
	
}

int nextpow2(int x) 
{
	int r = 1; 
	while (r < x) 
		r <<= 1; 
	return r; 
}

void __mia_glvolume_set_volume(MiaGLVolume *self)
{
	GLenum error; 

	glBindTexture(GL_TEXTURE_3D, self->volume_tex);
	error = glGetError();
	if (error)
		g_warning("set texture 0 error %s", gluErrorString(error));
	

	glTexImage3D (GL_TEXTURE_3D, 0, GL_ALPHA,
		      self->nx, self->ny, self->nz, 0, GL_ALPHA, 
		      GL_FLOAT, self->pixels);

	error = glGetError();
	if (error)
		g_warning("set texture 0 error %s", gluErrorString(error));

	
	g_free(self->pixels);
	self->pixels = NULL; 
}

static void
__mia_glvolume_set_property (GObject * object,
			     guint property_id,
			     const GValue * UNUSED(value), GParamSpec * pspec)
{
	switch (property_id) {
		/* handle your properties here */
	default:
		/* We don't have any other property... */
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id,
						   pspec);
	}
}

static void
__mia_glvolume_get_property (GObject * object,
			     guint property_id,
			     GValue * UNUSED(value), GParamSpec * pspec)
{
	switch (property_id) {
		/* handle your properties here */
	default:
		/* We don't have any other property... */
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id,
						   pspec);
	}
}

static void
__mia_glvolume_instance_init (GTypeInstance * instance, gpointer UNUSED(g_class))
{
	MiaGLDrawable *drawable = (MiaGLDrawable *)instance;
	MiaGLVolume *self = (MiaGLVolume *) instance;
	self->dispose_has_run = FALSE;
	self->pixels = NULL; 
	g_mutex_init(&self->pixel_mutex);
	drawable->is_transparent = FALSE; 
}

static GObject *
__mia_glvolume_constructor (GType type,
			    guint n_construct_properties,
			    GObjectConstructParam * construct_properties)
{
	GObject *obj;
	{
		/* Invoke parent constructor. */
		MiaGLVolumeClass *klass;
		GObjectClass *parent_class;
		klass = MIA_GLVOLUME_CLASS (g_type_class_peek
					    (MIA_TYPE_GLVOLUME));
		parent_class =
			G_OBJECT_CLASS (g_type_class_peek_parent (klass));
		obj = parent_class->constructor (type, n_construct_properties,
						 construct_properties);
	}

	/* add your code here */

	return obj;
}

static void
__mia_glvolume_dispose (GObject * obj)
{
	MiaGLVolumeClass *klass;
	GObjectClass *parent_class;
	MiaGLVolume *self = (MiaGLVolume *) obj;

	if (self->dispose_has_run) {
		/* If dispose did already run, return. */
		return;
	}
	/* Make sure dispose does not run twice. */
	self->dispose_has_run = TRUE;

	/* add your code here */
	if (self->light_direction)
		g_object_unref(self->light_direction);
	
	g_object_unref(self->iso_range_max);
	g_object_unref(self->iso_range_min);

	/* chain up to the parent class */
	klass = MIA_GLVOLUME_CLASS (g_type_class_peek (MIA_TYPE_GLVOLUME));
	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	parent_class->dispose (obj);
}

static void
__mia_glvolume_finalize (GObject * obj)
{
	MiaGLVolumeClass *klass;
	GObjectClass *parent_class;
	MiaGLVolume *self = (MiaGLVolume *) obj;

	/* add your destruction code here */
	if (self->pixels)
		g_free(self->pixels);
	

	/* chain up to the parent class */
	klass = MIA_GLVOLUME_CLASS (g_type_class_peek (MIA_TYPE_GLVOLUME));
	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	parent_class->finalize (obj);
}



static void
__mia_glvolume_class_init (gpointer g_class, gpointer UNUSED(g_class_data))
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
	MiaGLDrawableClass *klass = MIA_GLDRAWABLE_CLASS(g_class);
	
	gobject_class->set_property = __mia_glvolume_set_property;
	gobject_class->get_property = __mia_glvolume_get_property;
	gobject_class->dispose = __mia_glvolume_dispose;
	gobject_class->finalize = __mia_glvolume_finalize;
	gobject_class->constructor = __mia_glvolume_constructor;

	klass->gl_attach = __gl_attach; 
	klass->gl_detach = __gl_detach; 
	klass->gl_draw   = __gl_draw; 
	klass->initialize = __initialize; 
	/* add your code here (e.g. define properties) */
	

}
static gboolean cg_sucess = TRUE;


static const char *__vertex_program[] = {
	"uniform vec3 GradientDelta;\n",
	"void main(void)\n",
	"{\n",
	"  gl_TexCoord[0]=gl_MultiTexCoord0;\n",
	"  gl_TexCoord[1]=vec4(gl_MultiTexCoord0.r + GradientDelta.r, gl_MultiTexCoord0.gba);\n",
	"  gl_TexCoord[2]=vec4(gl_MultiTexCoord0.r, gl_MultiTexCoord0.g +GradientDelta.g, gl_MultiTexCoord0.ba);\n",
	"  gl_TexCoord[3]=vec4(gl_MultiTexCoord0.rg, gl_MultiTexCoord0.b +GradientDelta.b,gl_MultiTexCoord0.a);\n",
	"  gl_Position = ftransform();\n",
	"}\n"
};

static const char *__fragment_program[] = {
	"uniform vec3 GradientScale;\n",
	"uniform vec3 LightDirection;\n",
	"uniform sampler3D VolumeTexture;\n",

	"uniform float alpha_discard_max;\n"
	"uniform float alpha_discard_min;\n"
	"void main(void)\n",
	"{\n",
	"  float alpha = texture3D(VolumeTexture, gl_TexCoord[0].xyz).a;\n", //getting the Normal/Value Values,
	"  if (alpha > alpha_discard_max || alpha < alpha_discard_min) discard;\n", 
	//computing SurfaceNormal
	"  float nx = GradientScale.x * (texture3D(VolumeTexture, gl_TexCoord[1].xyz).a - alpha);\n",
	"  float ny = GradientScale.y * (texture3D(VolumeTexture, gl_TexCoord[2].xyz).a - alpha);\n",
	"  float nz = GradientScale.z * (texture3D(VolumeTexture, gl_TexCoord[3].xyz).a - alpha);\n",
	"  vec3 Norm = normalize(vec3(nx, ny, nz));",          
	"  float LightIntens =  max(dot(LightDirection, Norm),0.0);",     //computing Light Intensity
	"  gl_FragColor = vec4(LightIntens, LightIntens, LightIntens, alpha);",               //setting Fragmentcolor
	"}\n"
};	

char info[3000];

void gl_check_shader(GLint shader)
{
	int success = 0; 
	glGetObjectParameterivARB(shader,GL_OBJECT_COMPILE_STATUS_ARB,&success);
	
	if(!success){
		int info_lengh = 0; 
		glGetObjectParameterivARB(shader,GL_OBJECT_INFO_LOG_LENGTH_ARB, &info_lengh); 
		g_warning("Shader compilation failed, log of length %d follows:", info_lengh); 
		glGetInfoLogARB(shader,3000,NULL,info);
		g_warning("%s", info); 
        }
}; 

static void
__gl_attach (MiaGLDrawable * obj)
{
	GLenum error; 
	MiaGLVolume *self = MIA_GLVOLUME (obj);

	cg_sucess = TRUE;
	
	
	glGenTextures(1, &self->volume_tex);
	
	error = glGetError();
	if (error) {
		g_warning("gen texture error %s", gluErrorString(error));
		return; 
	}

	g_mutex_lock(&self->pixel_mutex);
	if (self->pixels)
		__mia_glvolume_set_volume(self);

	self->cg_vtxprog  = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
	glShaderSourceARB(self->cg_vtxprog, sizeof(__vertex_program)/sizeof(char*), __vertex_program, NULL);
	glCompileShaderARB(self->cg_vtxprog);
	gl_check_shader(self->cg_vtxprog); 

	self->cg_fragprog = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
	glShaderSourceARB(self->cg_fragprog, sizeof(__fragment_program)/sizeof(char*), __fragment_program, NULL);
	glCompileShaderARB(self->cg_fragprog);
	gl_check_shader(self->cg_fragprog); 
	
	
	self->shader_program = glCreateProgramObjectARB();

	error = glGetError ();
	if (error)
		g_warning ("Create program object: error %s", gluErrorString (error));

	

	

	glAttachObjectARB(self->shader_program,self->cg_vtxprog);
	error = glGetError ();
	if (error)
		g_warning ("attach vertex program: error %s", gluErrorString (error));

	glAttachObjectARB(self->shader_program,self->cg_fragprog);
	
	error = glGetError ();
	if (error)
		g_warning ("attach fragment program: error %s", gluErrorString (error));

	glLinkProgramARB(self->shader_program);
	
	self->cg_light_direction_f3 = glGetUniformLocationARB(self->shader_program, "LightDirection");
	error = glGetError ();
	if (error)
		g_warning ("get 'LightDirection': error %s", gluErrorString (error));

	self->cg_delta_f3 = glGetUniformLocationARB(self->shader_program, "GradientDelta");
	error = glGetError ();
	if (error)
		g_warning ("get 'GradientDelta': error %s", gluErrorString (error));

	self->cg_scale_f3 = glGetUniformLocationARB(self->shader_program, "GradientScale");
	error = glGetError ();
	if (error)
		g_warning ("get 'GradientScale': error %s", gluErrorString (error));

	
	self->cg_alpha_discard_min = glGetUniformLocationARB(self->shader_program, "alpha_discard_min");
	error = glGetError ();
	if (error)
		g_warning ("get 'alpha_discard': error %s", gluErrorString (error));

	self->cg_alpha_discard_max = glGetUniformLocationARB(self->shader_program, "alpha_discard_max");
	error = glGetError ();
	if (error)
		g_warning ("get 'alpha_discard': error %s", gluErrorString (error));


	g_mutex_unlock(&self->pixel_mutex);	

	obj->is_attached = TRUE;
	obj->is_visible = TRUE;
}

static void
__gl_detach (MiaGLDrawable * obj)
{
	MiaGLVolume *self;
	if (!obj->is_attached)
		return;

	self = MIA_GLVOLUME (obj);
	glDeleteTextures(1, &self->volume_tex);
	
	
	obj->is_attached = FALSE;
}

static void
__prepar_cg_env (MiaGLVolume * self, MiauiGLView * view)
{
	GLenum error;

	self->light_direction =
		miaui_glview_get_light_direction (view,
						  self->light_direction);

	glUseProgramObjectARB(self->shader_program);
	error = glGetError ();
	if (error)
		g_warning ("__prepar_cg_env glUseProgramObjectARB: error %s", gluErrorString (error));

	
	glUniform3fARB(self->cg_light_direction_f3, 
		       self->light_direction->x, 
		       self->light_direction->y, 
		       self->light_direction->z
		       );

	error = glGetError ();
	if (error)
		g_warning ("__prepar_cg_env set light direction: error %s", gluErrorString (error));

	glUniform3fARB(self->cg_delta_f3, 
		       1.0 / self->nx, 
		       1.0 / self->ny, 
		       1.0 / self->nz
		       );
	
	error = glGetError ();
	if (error)
		g_warning ("__prepar_cg_env set delta: error %s", gluErrorString (error));


	glUniform3fARB(self->cg_scale_f3, 
		       1.0 / self->nx, 
		       1.0 / self->ny, 
		       1.0 / self->nz
		       );

	error = glGetError ();
	if (error)
		g_warning ("__prepar_cg_env set scale: error %s", gluErrorString (error));
		

	glUniform1fARB(self->cg_alpha_discard_min, iso_value_min / self->iso_max); 
	glUniform1fARB(self->cg_alpha_discard_max, iso_value_max / self->iso_max); 

	error = glGetError ();
	if (error)
		g_warning ("__prepar_cg_env set alpha_discard: error %s", gluErrorString (error));
	
}

static void
__release_cg_env (MiaGLVolume * UNUSED(self))
{

	glUseProgramObjectARB(0);
}


static inline void
__enable_texture (MiaGLVolume * self)
{
	glEnable (GL_TEXTURE_3D);
	glBindTexture (GL_TEXTURE_3D, self->volume_tex);

	glTexParameterf (GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf (GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP);
	glTexParameterf (GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf (GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf (GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexEnvf (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

}

static inline void
__disable_texture (MiaGLVolume * UNUSED(self))
{
	glDisable (GL_TEXTURE_3D);
}

gboolean get_render_abort()
{
	return miaui_glview_check_abort_rendering(MIAUI_GLVIEW(glview));
}

static inline void
__draw_xslice(MiaGLVolume *self, float x)
{
	gfloat fx = 2 * x * self->sx; 
	gfloat fy = 2 * self->sy; 
	gfloat fz = 2 * self->sz; 

	glBegin (GL_QUADS);
	glTexCoord3f (x, 0.0, 0.0);
	glVertex3f ( fx, 0.0, 0.0);

	glTexCoord3f (x, 0.0f, 1.0);
	glVertex3f ( fx, 0.0, fz);

	glTexCoord3f (x, 1.0f, 1.0);
	glVertex3f ( fx, fy,  fz);

	glTexCoord3f (x, 1.0f, 0.0);
	glVertex3f ( fx, fy, 0.0);
	glEnd ();
}

void
__glview_draw_x_slices_p (MiaGLVolume *self, float step, gboolean high)
{
	float z;
	for (z = 0.0; z <= 1.0; z += step) {
		__draw_xslice(self,z);
		if (high && get_render_abort ())
			break;
	}
}

void
__glview_draw_x_slices_m (MiaGLVolume *self, float step, gboolean high)
{
	float z;
	for (z = 1.0; z >=0.0; z -= step) {
		__draw_xslice(self,z);
		if (high && get_render_abort ())
			break;
	}
}

static inline void 
__draw_yslice(MiaGLVolume *self, float y)
{
	gfloat fx = 2 * self->sx; 
	gfloat fy = 2 * y * self->sy; 
	gfloat fz = 2 * self->sz; 

	glBegin (GL_QUADS);
	glTexCoord3f (0.0f, y, 0.0);
	glVertex3f ( 0.0, fy, 0.0);

	glTexCoord3f (0.0f, y, 1.0);
	glVertex3f ( 0.0, fy, fz);

	glTexCoord3f (1.0f, y, 1.0);
	glVertex3f (fx, fy, fz);

	glTexCoord3f (1.0f, y, 0.0);
	glVertex3f (fx, fy, 0.0);
	glEnd ();	
}

void
__glview_draw_y_slices_m (MiaGLVolume *self, float step, gboolean high)
{
	float z;
	for (z = 1.0;  z >= 0.0; z -= step) {
		__draw_yslice(self,z);
		if (high && get_render_abort ())
			break;
	}
}

void
__glview_draw_y_slices_p (MiaGLVolume *self, float step, gboolean high)
{
	float z; 
	for (z = 0.0; z <= 1.0; z += step) {
		__draw_yslice(self,z);
		if (high && get_render_abort ())
				break;
	}
}

static inline void 
__draw_zslice(MiaGLVolume *self, float z)
{
	gfloat fx = 2 * self->sx; 
	gfloat fy = 2 * self->sy; 
	gfloat fz = 2 * z * self->sz;
	
	glBegin (GL_QUADS);
	glTexCoord3f (0.0f, 0.0, z);
	glVertex3f ( 0.0, 0.0, fz);

	glTexCoord3f (0.0f, 1.0, z);
	glVertex3f (0.0, fy, fz);

	glTexCoord3f (1.0f, 1.0, z);
	glVertex3f (fx, fy, fz);

	glTexCoord3f (1.0f, 0.0, z);
	glVertex3f (fx, 0.0, fz);
	glEnd ();
}

void
__glview_draw_z_slices_m (MiaGLVolume *self, float step, gboolean high)
{
	float z; 
	for (z = 1.0; z >=0.0; z -= step) {
		__draw_zslice(self,z);
		if (high && get_render_abort ())
			break;
	}
}

void
__glview_draw_z_slices_p (MiaGLVolume *self, float step, gboolean high)
{
	float z;
	for (z = 0.0; z <= 1.0; z += step) {
		__draw_zslice(self,z);
		if (high && get_render_abort ())
			break;
	}
}

static void 
__initialize(MiaGLDrawable *obj)
{
	
	MiaGLVolume *self = MIA_GLVOLUME (obj);
	if (!obj->is_attached)
		return;
	g_mutex_lock(&self->pixel_mutex);
	if (self->pixels)
		__mia_glvolume_set_volume(self);
	g_mutex_unlock(&self->pixel_mutex);
}


static void
__gl_draw (MiaGLDrawable * obj, MiauiGLView * view, gboolean high_resolution)
{
	MiaGLVolume *self; 
	GLenum error; 
	float z_step; 
	if (!obj->is_attached) {
		g_warning ("try to draw an un-attached object");
		return;
	}
	if (!obj->is_visible)
		return;

	self = MIA_GLVOLUME(obj);
	
	
	if (!self->volume_tex)
		return;
	
	z_step = high_resolution ? 1.0f/2048.0f : 1.0f / 256.0f;  
	
	__enable_texture (self);
	__prepar_cg_env (self, view);


	switch (miaui_glview_get_draw_direction (view)) {
	case od_xm:
		__glview_draw_x_slices_m (self, z_step, high_resolution);
		break;
	case od_xp:
		__glview_draw_x_slices_p (self, z_step, high_resolution);
		break;
	case od_ym:
		__glview_draw_y_slices_m (self, z_step, high_resolution);
		break;
	case od_yp:
		__glview_draw_y_slices_p (self, z_step, high_resolution);
		break;
	case od_zm:
		__glview_draw_z_slices_m (self, z_step, high_resolution);
		break;
	case od_zp:
		__glview_draw_z_slices_p (self, z_step, high_resolution);
		break;
	default:
		g_warning("bougus optimal direction, not dtawing anything");
	}

	__release_cg_env (self);
	__disable_texture (self);
	
	error = glGetError();
	if (error)
		g_warning("volume_renderer error %s", gluErrorString(error));
	
}

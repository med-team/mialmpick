/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gtk/gtk.h>
#include "globals.h"
#include "landmarkinput.h"


struct _MiauiLandmarkInput {
	GtkWindow parent;
	gboolean dispose_has_run;
	/* add your class instance elements here */
	GtkEntry *entry_landmark_name;
	GtkButton *button_cancellm;
	GtkButton *button_addlm;
	MiaLandmark *lm;
	miaui_landmarkinput_callback callback;
	MiauiLandmarkTable *lm_table;
	GtkWidget *glview;
};


static GObject *__miaui_landmark_input_constructor (GType type,
						    guint
						    n_construct_properties,
						    GObjectConstructParam *
						    construct_properties);


static void
  __miaui_landmark_input_class_init (gpointer g_class, gpointer g_class_data);

static void
  __miaui_landmark_input_instance_init (GTypeInstance * instance,
					gpointer g_class);

GType
miaui_landmark_input_get_type (void)
{
	static GType type = 0;
	if (type == 0)
	{
		static const GTypeInfo info = {
			sizeof (MiauiLandmarkInputClass),
			NULL,	/* base_init */
			NULL,	/* base_finalize */
			__miaui_landmark_input_class_init,	/* class_init */
			NULL,	/* class_finalize */
			NULL,	/* class_data */
			sizeof (MiauiLandmarkInput),
			0,	/* n_preallocs */
			__miaui_landmark_input_instance_init, /* instance_init */
			NULL
		};
		type = g_type_register_static (GTK_TYPE_WINDOW,
					       "MiauiLandmarkInputType",
					       &info, 0);
	}
	return type;
}

static void
  __miaui_landmark_input_add_callback (GtkWidget * button, MiauiLandmarkInput * lmi);


MiauiLandmarkInput *
miaui_landmark_input_new (gchar * titel, MiaLandmark *lm)
{
	MiauiLandmarkInput *obj =
		g_object_new (MIAUI_TYPE_LANDMARK_INPUT, NULL);
	/* add your code here */
	gtk_window_set_title (GTK_WINDOW (obj), _(titel));
	obj->lm = lm;

	if (lm)
		gtk_entry_set_text (obj->entry_landmark_name, 
				    mia_landmark_get_name(lm));

	return obj;
}

void
miaui_landmark_input_set_add_callback (MiauiLandmarkInput * self,
				       miaui_landmarkinput_callback
				       callback, MiauiLandmarkTable *table, GtkWidget *glview)
{
	MIAUI_IS_LANDMARK_INPUT (self);
	self->lm_table = table;
	self->callback = callback;
	self->glview = glview;  
}

static void

__miaui_landmark_input_set_property (GObject * object,
				     guint property_id,
				     const GValue * UNUSED(value), GParamSpec * pspec)
{
	switch (property_id)
	{
		/* handle your properties here */
	default:
		/* We don't have any other property... */
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object,
						   property_id, pspec);
	}
}

static void

__miaui_landmark_input_add_callback (GtkWidget * UNUSED(button),
				     MiauiLandmarkInput * lmi)
{
	const gchar *newname; 
	MIAUI_IS_LANDMARK_INPUT (lmi);
	/* get name and iso-value from the dialog */

	newname = gtk_entry_get_text (lmi->entry_landmark_name);
	lmi->callback (newname, lmi->lm, lmi->lm_table, lmi->glview);

	gtk_widget_destroy (GTK_WIDGET (lmi));
}

static void

__miaui_landmark_input_get_property (GObject * object,
				     guint property_id,
				     GValue * UNUSED(value), GParamSpec * pspec)
{
	switch (property_id)
	{
		/* handle your properties here */
	default:
		/* We don't have any other property... */
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object,
						   property_id, pspec);
	}
}

static void

__miaui_landmark_input_instance_init (GTypeInstance *instance, gpointer UNUSED(g_class))
{
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *table;
	GtkWidget *label;

	MiauiLandmarkInput *self = (MiauiLandmarkInput *) instance;
	self->dispose_has_run = FALSE;

	gtk_window_set_modal (GTK_WINDOW (self), TRUE);
	gtk_window_set_position (GTK_WINDOW (self), GTK_WIN_POS_MOUSE);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (self), vbox);

	table = gtk_table_new (3, 2, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

	label = gtk_label_new (_("Name: "));
	gtk_widget_show (label);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);

	self->entry_landmark_name = GTK_ENTRY (gtk_entry_new ());
	gtk_widget_show (GTK_WIDGET (self->entry_landmark_name));
	gtk_table_attach (GTK_TABLE (table),
			  GTK_WIDGET (self->entry_landmark_name), 1,
			  2, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

	self->button_addlm =
		GTK_BUTTON (gtk_button_new_from_stock (GTK_STOCK_APPLY));

	GTK_WIDGET_SET_FLAGS (self->button_addlm, GTK_HAS_DEFAULT|GTK_CAN_DEFAULT);

	gtk_widget_show (GTK_WIDGET (self->button_addlm));
	gtk_box_pack_start (GTK_BOX (hbox),
			    GTK_WIDGET (self->button_addlm), FALSE, FALSE, 0);

	g_signal_connect (G_OBJECT (self->button_addlm),
			  "clicked",
			  G_CALLBACK
			  (__miaui_landmark_input_add_callback), self);

	self->button_cancellm =
		GTK_BUTTON (gtk_button_new_from_stock ("gtk-cancel"));
	gtk_widget_show (GTK_WIDGET (self->button_cancellm));
	g_signal_connect_swapped (G_OBJECT (self->button_cancellm),
				  "clicked",
				  G_CALLBACK (gtk_widget_destroy),
				  (gpointer) self);
	
	gtk_box_pack_start (GTK_BOX (hbox),
			    GTK_WIDGET (self->button_cancellm), FALSE,
			    FALSE, 0);



	self->callback = NULL;
}

static GObject *
__miaui_landmark_input_constructor (GType type,
				    guint
				    n_construct_properties,
				    GObjectConstructParam
				    * construct_properties)
{
	GObject *obj;
	{
		/* Invoke parent constructor. */
		MiauiLandmarkInputClass *klass;
		GObjectClass *parent_class;
		klass = MIAUI_LANDMARK_INPUT_CLASS (g_type_class_peek
						    (MIAUI_TYPE_LANDMARK_INPUT));
		parent_class =
			G_OBJECT_CLASS (g_type_class_peek_parent (klass));
		obj = parent_class->constructor (type,
						 n_construct_properties,
						 construct_properties);
	}

	/* add your code here */

	return obj;
}

static void
__miaui_landmark_input_dispose (GObject * obj)
{
	MiauiLandmarkInputClass *klass;
	GObjectClass *parent_class;

	MiauiLandmarkInput *self = (MiauiLandmarkInput *) obj;

	if (self->dispose_has_run)
	{
		/* If dispose did already run, return. */
		return;
	}
	/* Make sure dispose does not run twice. */
	self->dispose_has_run = TRUE;



	klass = MIAUI_LANDMARK_INPUT_CLASS (g_type_class_peek
					    (MIAUI_TYPE_LANDMARK_INPUT));
	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	parent_class->dispose (obj);

}

static void

__miaui_landmark_input_class_init (gpointer g_class, gpointer UNUSED(g_class_data))
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);

	gobject_class->set_property = __miaui_landmark_input_set_property;
	gobject_class->get_property = __miaui_landmark_input_get_property;
	gobject_class->dispose = __miaui_landmark_input_dispose;
	gobject_class->constructor = __miaui_landmark_input_constructor;

	/* add your code here (e.g. define properties) */

}



gboolean
miaui_landmark_input_test (void)
{
	gboolean result = FALSE;
	MiauiLandmarkInput *test = miaui_landmark_input_new ("test", NULL);
	g_return_val_if_fail (test, FALSE);
	result = MIAUI_IS_LANDMARK_INPUT (test);

	/* add your tests here */


	g_object_unref (G_OBJECT (test));
	return result;
}

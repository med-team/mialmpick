/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#ifndef __gllandmark_h
#define __gllandmark_h

#include <mialm/mialandmarklist.h>
#include "gldrawable.h"


#define MIA_TYPE_GLLANDMARK (mia_gllandmark_get_type())
#define MIA_GLLANDMARK(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIA_TYPE_GLLANDMARK, MiaGLLandmark))
#define MIA_GLLANDMARK_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIA_TYPE_GLLANDMARK, MiaGLLandmarkClass))
#define MIA_IS_GLLANDMARK(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIA_TYPE_GLLANDMARK))
#define MIA_IS_GLLANDMARK_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIA_TYPE_GLLANDMARK))
#define MIA_GLLANDMARK_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst), MIA_TYPE_GLLANDMARK ,MiaGLLandmarkClass))

extern const char mia_gllandmark_prop_list[];
extern const char mia_gllandmark_prop_color[];
		

typedef struct _MiaGLLandmark MiaGLLandmark;
typedef struct _MiaGLLandmarkClass MiaGLLandmarkClass;

struct _MiaGLLandmarkClass {
	MiaGLDrawableClass parent;
	void (*gl_attach)(MiaGLDrawable *self);
	void (*gl_detach)(MiaGLDrawable *self);
	void (*gl_draw)(MiaGLDrawable *self, gboolean high_resolution);
};

GType mia_gllandmark_get_type(void);

MiaGLLandmark *mia_gllandmark_new(gchar *name, MiaLandmarklist *lml);

gboolean mia_gllandmark_test(void);

#endif

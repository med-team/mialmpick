/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include <string.h>

#include "gllandmark.h"
#include "glview.h"

const char mia_gllandmark_prop_list[] = "list";
const char mia_gllandmark_prop_color[] = "color";



struct _MiaGLLandmark {
	MiaGLDrawable parent;
	gboolean dispose_has_run;
	/* add your class instance elements here */
	MiaLandmarklist *landmarks;
	MiaColor color;
	GLuint sphere_list; 
	GLfloat select_color[4];
	GLfloat standart_color[4];
	
};
	
static GObject *
__mia_gllandmark_constructor (GType                  type,
		    guint                  n_construct_properties,
		    GObjectConstructParam *construct_properties);


static void 
__mia_gllandmark_class_init (gpointer g_class,
                   gpointer g_class_data);

static void 
__mia_gllandmark_instance_init (GTypeInstance   *instance,
                      gpointer         g_class);

GType mia_gllandmark_get_type(void)
{
	static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MiaGLLandmarkClass),
                        NULL,   /* base_init */
                        NULL,   /* base_finalize */
                        __mia_gllandmark_class_init,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        sizeof (MiaGLLandmark),
                        0,      /* n_preallocs */
                        __mia_gllandmark_instance_init,    /* instance_init */
			NULL
                };
                type = g_type_register_static (MIA_TYPE_GLDRAWABLE,
                                               "MiaGLLandmarkType",
                                               &info, 0);
        }
        return type;	
}




MiaGLLandmark *mia_gllandmark_new(gchar *name, MiaLandmarklist *lml)
{
	MiaGLLandmark *obj = (MiaGLLandmark *)g_object_new(MIA_TYPE_GLLANDMARK,
				mia_gldrawable_name_property, name, 
				mia_gllandmark_prop_list, lml, 
			     NULL);
	/* add your code here */ 
	
	return obj; 
}

enum {
	prop_list = 1 
};

static void
__mia_gllandmark_set_property (GObject      *object,
                     guint         property_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
        MiaGLLandmark *self = (MiaGLLandmark *) object;
	switch (property_id) {
	/* handle your properties here */	
	case prop_list:
		if (G_IS_OBJECT(self->landmarks))
			g_object_unref(self->landmarks);
		self->landmarks = g_value_get_pointer(value);
		if (G_IS_OBJECT(self->landmarks))
			g_object_ref(self->landmarks);
		break;
	default:
                /* We don't have any other property... */
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
__mia_gllandmark_get_property (GObject      *object,
                     guint         property_id,
                     GValue       *value,
                     GParamSpec   *pspec)
{
        MiaGLLandmark *self = (MiaGLLandmark *) object;
	switch (property_id) {
        /* handle your properties here */
	case prop_list:
		g_value_set_pointer(value, self->landmarks);
		break; 
        default:
                /* We don't have any other property... */
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
__mia_gllandmark_instance_init (GTypeInstance   *instance,
				gpointer         UNUSED(g_class))
{
        MiaGLDrawable *drawable = (MiaGLDrawable *)instance; 
	MiaGLLandmark *self = (MiaGLLandmark *)instance;
        self->dispose_has_run = FALSE;
	self->select_color[0] = 0.0;
	self->select_color[1] = 0.5;
	self->select_color[2] = 1.0;
	self->select_color[3] = 0.7;
	
	self->standart_color[0] = 1.0;
	self->standart_color[1] = 1.0;
	self->standart_color[2] = 0.0;
	self->standart_color[3] = 0.7;
	drawable->is_transparent = TRUE; 
	
}

static GObject *
__mia_gllandmark_constructor (GType                  type,
                    guint                  n_construct_properties,
                    GObjectConstructParam *construct_properties)
{
        GObject *obj;
        {
                /* Invoke parent constructor. */
                MiaGLLandmarkClass *klass;
                GObjectClass *parent_class;  
                klass = MIA_GLLANDMARK_CLASS (g_type_class_peek (MIA_TYPE_GLLANDMARK));
                parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
                obj = parent_class->constructor (type,
                                                 n_construct_properties,
                                                 construct_properties);
        }
        
        /* add your code here */
	
        return obj;
}

static void __gl_attach(MiaGLDrawable *obj)
{
	GLUquadricObj *qobj; 
	MiaGLLandmark *self = MIA_GLLANDMARK(obj);
	
	self->sphere_list = glGenLists(1);
	if (!self->sphere_list) {
		g_warning("MiaGLLandmark: gl_attach: %s", gluErrorString(glGetError()));
		return; 
	}		
	
	qobj = gluNewQuadric();
	gluQuadricNormals(qobj,GLU_SMOOTH);
	gluQuadricDrawStyle(qobj,GLU_FILL);
	gluQuadricOrientation(qobj,GLU_OUTSIDE );
	
	glNewList(self->sphere_list,GL_COMPILE);
	gluSphere(qobj, 0.01, 20, 20 );
	glEndList();
	gluDeleteQuadric(qobj);
		
	obj->is_attached = TRUE; 
}

static void __gl_detach(MiaGLDrawable *obj)
{
	MiaGLLandmark *self = MIA_GLLANDMARK(obj);
	glDeleteLists(self->sphere_list, 1);
	obj->is_attached = FALSE; 
}

typedef struct _DrawData DrawData;
struct 	_DrawData {
	MiauiGLView *glview; 
	MiaGLLandmark  *landmarklist;
	gboolean high_resolution;
};

static void __draw_points(gpointer UNUSED(key), gpointer value, gpointer user_data)
{
	MiaVector3d *loc; 
	MiaLandmark *lm = MIA_LANDMARK(value);
	
	MiaGLLandmark *self  = MIA_GLLANDMARK(user_data);
	
	g_assert(lm);	
	
	glPushMatrix();
	loc = mia_landmark_get_location(lm);
	if (loc) {
		if (strcmp(mia_landmark_get_name(lm), 
			   mia_landmarklist_get_selected(self->landmarks))) {
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, self->standart_color); 	
		} else {
			glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, self->select_color); 	
		}	
		
		glTranslatef(loc->x / 128.0, loc->y / 128.0, loc->z/ 128.0);
		glCallList(self->sphere_list);
	}
	glPopMatrix();
}


static void __gl_draw(MiaGLDrawable *obj, MiauiGLView *UNUSED(view), gboolean UNUSED(high_resolution))
{
	GLfloat light_model = 1.0; 
	MiaGLLandmark *self = MIA_GLLANDMARK(obj);
	if (!obj->is_attached) {
		g_warning("try to draw an un-attached object");
		return; 
	}
	if (!obj->is_visible)
		return; 
	
	
	glLightModelfv(GL_LIGHT_MODEL_TWO_SIDE, &light_model);
	
	glMatrixMode(GL_MODELVIEW);
	
	if (self->landmarks)
		mia_landmarklist_foreach(self->landmarks, __draw_points, self );
	
	
	
}

static void
__mia_gllandmark_dispose (GObject *obj)
{
        MiaGLLandmarkClass *klass;
        GObjectClass *parent_class;
        MiaGLLandmark *self = (MiaGLLandmark *)obj;

        if (self->dispose_has_run) {
                /* If dispose did already run, return. */
                return;
        }
        /* Make sure dispose does not run twice. */
        self->dispose_has_run = TRUE;
	
	/* add your code here */
	if (G_IS_OBJECT(self->landmarks)) {
		g_object_unref(self->landmarks);
		self->landmarks = NULL;
	}
       
 /* chain up to the parent class */
        klass = MIA_GLLANDMARK_CLASS (g_type_class_peek
					    (MIA_TYPE_GLLANDMARK));
	parent_class =	G_OBJECT_CLASS(g_type_class_peek_parent (klass));
	parent_class->dispose(obj); 
}

static void
__mia_gllandmark_finalize (GObject *obj)
{
        MiaGLLandmarkClass *klass;
        GObjectClass *parent_class;
        /*MiaGLLandmark *self = (MiaGLLandmark *)obj;*/

        /* add your destruction code here */
	/* chain up to the parent class */
        klass = MIA_GLLANDMARK_CLASS (g_type_class_peek
					    (MIA_TYPE_GLLANDMARK));
	parent_class =	G_OBJECT_CLASS(g_type_class_peek_parent (klass));
	parent_class->finalize(obj); 
}

static void
__mia_gllandmark_class_init (gpointer g_class,
			     gpointer UNUSED(g_class_data))
{
	GParamSpec *pspec;
	MiaGLDrawableClass *klass = MIA_GLDRAWABLE_CLASS(g_class);
        GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);

	g_assert(klass);
	
        gobject_class->set_property = __mia_gllandmark_set_property;
        gobject_class->get_property = __mia_gllandmark_get_property;
        gobject_class->dispose = __mia_gllandmark_dispose;
        gobject_class->finalize = __mia_gllandmark_finalize;
        gobject_class->constructor = __mia_gllandmark_constructor;

	klass->gl_attach = __gl_attach; 
	klass->gl_detach = __gl_detach; 
	klass->gl_draw   = __gl_draw; 
	
	/* add your code here (e.g. define properties) */
	pspec = g_param_spec_pointer (mia_gllandmark_prop_list, 
				      "list","landmark list", 
				      G_PARAM_READWRITE);	
	g_object_class_install_property (gobject_class, prop_list, pspec);
}

gboolean mia_gllandmark_test(void)
{
	gboolean result = FALSE;
	MiaGLLandmark *test = mia_gllandmark_new("test", NULL);
	g_return_val_if_fail(test, FALSE);
	result = MIA_IS_GLLANDMARK(test);
	
	/* add your tests here */
	
	
	g_object_unref(G_OBJECT(test));
	return result;
}

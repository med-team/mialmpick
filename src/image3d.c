/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <mialm/miadefines.h>
#include "image3d.h"

static GObject *__mia_image3d_constructor (GType type,
					   guint n_construct_properties,
					   GObjectConstructParam *
					   construct_properties);


static void
__mia_image3d_class_init (gpointer g_class, gpointer g_class_data);

static void
__mia_image3d_instance_init (GTypeInstance * instance, gpointer g_class);

GType
mia_image3d_get_type (void)
{
	static GType type = 0;
	if (type == 0)
	{
		static const GTypeInfo info = {
			sizeof (MiaImage3dClass),
			NULL,	/* base_init */
			NULL,	/* base_finalize */
			__mia_image3d_class_init,	/* class_init */
			NULL,	/* class_finalize */
			NULL,	/* class_data */
			sizeof (MiaImage3d),
			0,	/* n_preallocs */
			__mia_image3d_instance_init, /* instance_init */
			NULL
		};
		type = g_type_register_static (G_TYPE_OBJECT,
					       "MiaImage3dType", &info, 0);
	}
	return type;
}

MiaImage3d *
__mia_image3d_new(gint nx, gint ny, gint nz)
{
	MiaImage3d *obj =
		(MiaImage3d *) g_object_new (MIA_TYPE_IMAGE3D, NULL);
	/* add your code here */
	obj->nx = nx; 
	obj->ny = ny; 
	obj->nz = nz; 
	obj->size = nx * ny * nz;
	obj->sx = obj->sy = obj->sz = 1.0; 
	obj->minmax_is_valid = FALSE; 
	return obj; 
}

MiaImage3d *
mia_image3d_new(gint nx, gint ny, gint nz)
{
	MiaImage3d *obj = __mia_image3d_new(nx, ny, nz);
	obj->data = g_new( gfloat, obj->size * sizeof(gfloat));
	return obj; 
}


MiaImage3d *
mia_image3d_new0(gint nx, gint ny, gint nz)
{
	MiaImage3d *obj = __mia_image3d_new(nx, ny, nz);
	obj->data = g_new0( gfloat, obj->size * sizeof(gfloat));
	return obj; 
}

MiaImage3d *
mia_image3d_new_ubyte (gint nx, gint ny, gint nz, guint8 *data)
{
	gint i; 
	gfloat *outp; 
	guint8 *inp;

	MiaImage3d *obj = __mia_image3d_new(nx,ny,nz);
	g_assert(data);
	
	obj->data = g_new( gfloat, obj->size * sizeof(gfloat));
	outp = obj->data; 
	inp = data; 
	i = obj->size; 
	obj->min = obj->max = *inp;
	while (i--) {
		*outp = *inp++; 
		if (*outp > obj->max) obj->max = *outp; 
		if (*outp < obj->min) obj->min = *outp; 
		outp++; 	
	}
	obj->minmax_is_valid = TRUE;
	return obj;
}

MiaImage3d *
mia_image3d_new_short (gint nx, gint ny, gint nz, gint16 *data)
{
	gint i; 
	gfloat *outp; 
	gint16 *inp;

	MiaImage3d *obj = __mia_image3d_new(nx,ny,nz);
	g_assert(data);
	
	obj->data = g_new( gfloat, obj->size * sizeof(gfloat));
	outp = obj->data; 
	inp = data; 
	i = obj->size; 
	obj->min = obj->max = *inp;
	while (i--) {
		
			
		*outp = (*inp > 0 ) ? *inp : 0; 
		if (*outp > obj->max) obj->max = *outp; 
		if (*outp < obj->min) obj->min = *outp; 
		++outp; 
		++inp; 
	}
	obj->minmax_is_valid = TRUE; 

	return obj;
}


guint32 mia_image3d_get_flat_size(MiaImage3d *self)
{
	g_assert(MIA_IS_IMAGE3D(self));
	return self->size; 
}

void 
mia_image3d_set_scales(MiaImage3d *self, gfloat sx, gfloat sy, gfloat sz)
{
	g_assert(self); 
	self->sx = sx; 
	self->sy = sy; 
	self->sz = sz;
}

int minpow2_greater_x_less_n(int x, int n) 
{
	int r = 1; 
	while (r < x && r < n) {
		r <<=1; 
	}
	return r; 
}

MiaImage3d *
mia_image3d_crop_pow2(MiaImage3d *src, gint n)
{
	gfloat *src_data, *trgt_data ;
	MiaImage3d *trgt; 
	gint mx, my, mz;
	guint y,z;
		
	g_assert(MIA_IS_IMAGE3D(src));
	
	
	mx = minpow2_greater_x_less_n(src->nx, n); 
	my = minpow2_greater_x_less_n(src->ny, n); 
	mz = minpow2_greater_x_less_n(src->nz, n); 
	
//	ssy = src->ny > my ? (src->ny - my) * src->nx : 0; 
		
	//tsy = n > my ? (n - my) * n : 0;  
	
	trgt = mia_image3d_new(mx, my, mz);
	trgt->min = src->min; 
	trgt->max = src->max; 
	trgt->minmax_is_valid = src->minmax_is_valid; 	
	
	trgt_data = trgt->data; 
	src_data = src->data; 
	g_message("Got Input image of size %d %d %d:%p",  src->nx,src->ny,src->nz, trgt_data); 
	g_message("Created image of size %d %d %d:(%p - %p)",  mx,my,mz, trgt_data, trgt_data+ mx*my*mz); 
	
	for (z = 0; z < src->nz; z++) {
		for (y = 0; y < src->ny; y++) {
//			g_message("copy from %p to %p", src_data,trgt_data); 
			memcpy(trgt_data, src_data, src->nx * sizeof(gfloat));
			trgt_data += mx;
			src_data += src->nx; 
		}
		trgt_data += (my - src->ny) * mx; 
		//src_data += ssy;
	}
	
	
	return trgt; 
}

void mia_image3d_get_minmax(MiaImage3d *self, gfloat *min, gfloat *max)
{
	
	if (self->minmax_is_valid) {
		*min = self->min; 
		*max = self->max; 
	}else {
		gint32 n = self->size-1;
		gfloat  *p = self->data; 
		*min = *max = *p++; \
		while (n--) {
			if (*min > *p) *min = *p;
			if (*max < *p) *max = *p; 
			++p;
		}
		self->min = *min; 
		self->max = *max; 
		self->minmax_is_valid = TRUE;
	}
}

static void
__mia_image3d_set_property (GObject * object,
			    guint property_id,
			    const GValue * UNUSED(value), GParamSpec * pspec)
{
	switch (property_id)
	{
		/* handle your properties here */
	default:
		/* We don't have any other property... */
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id,
						   pspec);
	}
}

static void
__mia_image3d_get_property (GObject * object,
			    guint property_id,
			    GValue * UNUSED(value), GParamSpec * pspec)
{
	switch (property_id)
	{
		/* handle your properties here */
	default:
		/* We don't have any other property... */
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id,
						   pspec);
	}
}

static void
__mia_image3d_instance_init (GTypeInstance * UNUSED(instance), gpointer UNUSED(g_class))
{
}

static GObject *
__mia_image3d_constructor (GType type,
			   guint n_construct_properties,
			   GObjectConstructParam * construct_properties)
{
	GObject *obj;
	{
		/* Invoke parent constructor. */
		MiaImage3dClass *klass;
		GObjectClass *parent_class;
		klass = MIA_IMAGE3D_CLASS (g_type_class_peek
					   (MIA_TYPE_IMAGE3D));
		parent_class =
			G_OBJECT_CLASS (g_type_class_peek_parent (klass));
		obj = parent_class->constructor (type, n_construct_properties,
						 construct_properties);
	}

	/* add your code here */

	return obj;
}

static void
__mia_image3d_dispose (GObject * obj)
{
	MiaImage3dClass *klass;
	GObjectClass *parent_class;

	/* chain up to the parent class */
	klass = MIA_IMAGE3D_CLASS (g_type_class_peek (MIA_TYPE_IMAGE3D));
	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	parent_class->dispose (obj);
}

static void
__mia_image3d_finalize (GObject * obj)
{
	MiaImage3dClass *klass;
	GObjectClass *parent_class;
	MiaImage3d *self = (MiaImage3d *) obj;

	/* add your destruction code here */
	g_free(self->data);
	
	/* chain up to the parent class */
	klass = MIA_IMAGE3D_CLASS (g_type_class_peek (MIA_TYPE_IMAGE3D));
	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	parent_class->finalize (obj);
}

static void
__mia_image3d_class_init (gpointer g_class, gpointer UNUSED(g_class_data))
{
	GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);

	gobject_class->set_property = __mia_image3d_set_property;
	gobject_class->get_property = __mia_image3d_get_property;
	gobject_class->dispose = __mia_image3d_dispose;
	gobject_class->finalize = __mia_image3d_finalize;
	gobject_class->constructor = __mia_image3d_constructor;

	/* add your code here (e.g. define properties) */

}

gboolean
mia_image3d_test (void)
{
	gboolean result = FALSE;
	MiaImage3d *test = mia_image3d_new (10,10,10);
	g_return_val_if_fail (test, FALSE);
	result = MIA_IS_IMAGE3D (test);

	/* add your tests here */


	g_object_unref (G_OBJECT (test));
	return result;
}

/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __globals_h
#define __globals_h

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libintl.h>
//#include <gnome.h>

#include "landmarktable.h"

#define _(X) gettext(X)

extern GtkWidget *mainwindow;
extern GladeXML *xml;
extern GtkWidget *glview; 
extern MiauiLandmarkTable *landmarktable; 
extern float iso_value_max; 
extern float iso_value_min; 
extern GtkRange *range_iso_value_max; 
extern GtkRange *range_iso_value_min; 
extern const char *in_filename; 
extern const char *lm_filename; 
extern const char *out_filename;

extern int snap_number; 

G_BEGIN_DECLS 

void global_show_error_message(const gchar *format, ...);

G_END_DECLS

#endif

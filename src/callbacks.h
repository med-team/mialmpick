/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <gtk/gtk.h>

void
on_open_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_rb_rotate_toggled                   (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_rb_shift_toggled                    (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_rb_zoom_toggled                     (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_vscale_iso_value_changed            (GtkRange        *range,
                                        gpointer         user_data);

gboolean
on_da_volrend_button_press_event       (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_da_volrend_button_release_event     (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_da_volrend_configure_event          (GtkWidget       *widget,
                                        GdkEventConfigure *event,
                                        gpointer         user_data);

void
on_da_volrend_realize                  (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_da_volrend_unrealize                (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean
on_da_volrend_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_da_volrend_key_release_event        (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_button_loadset_clicked              (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_mainwindow_destroy_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_da_volrend_parent_set               (GtkWidget       *widget,
                                        GtkWidget       *old_parent,
                                        gpointer         user_data);

gboolean
on_da_volrend_button_press_event       (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_da_volrend_button_release_event     (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_da_volrend_configure_event          (GtkWidget       *widget,
                                        GdkEventConfigure *event,
                                        gpointer         user_data);

gboolean
on_da_volrend_motion_notify_event      (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data);

void
on_da_volrend_realize                  (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_da_volrend_unrealize                (GtkWidget       *widget,
                                        gpointer         user_data);

GtkWidget*
cvolrend (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

gboolean
on_da_volrend_motion_notify_event      (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data);

gboolean
on_da_volrend_expose_event             (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

GtkWidget*
glade_custum_creator (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

gboolean
on_mainwindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_add_landmark_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_del_landmark_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_save_landmarks_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_use_iso_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_use_camera_clicked           (GtkButton       *button,
                                        gpointer         user_data);

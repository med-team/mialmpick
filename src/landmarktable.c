/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <errno.h>
#include <gtk/gtk.h>

#include <mialm.h>

#include "landmarktable.h"
#include "globals.h"
#include "landmarkinput.h"
#include "gllandmark.h"

#include <string.h>

enum{
	COL_NAME = 0,
	COL_LOCATION,
	NUM_COLS
};


struct _MiauiLandmarkTable {
	GtkVBox parent;
	gboolean dispose_has_run;
	/* add your class instance elements here */
	GtkTreeView *tv;
	GtkTreeSelection *selection;
	MiaLandmarklist *lml;
	GtkEntry *name_entry;
	GtkLabel *file_label; 
	GtkToggleButton *advancemode; 
	GtkToggleButton *cameramode;
	GtkToggleButton *isomode;
	GtkImage *template;
	
	GtkFileSelection *fs;
	GString *lml_filename;
	GtkWidget *landmark_menu;
	gboolean dirty; 
	MiaGLLandmark *gl_landmarks;
};



static GObject *__miaui_landmark_table_constructor (GType type,
						    guint
						    n_construct_properties,
						    GObjectConstructParam *
						    construct_properties);


static void
__miaui_landmark_table_class_init (gpointer g_class, gpointer g_class_data);

static void
__miaui_landmark_table_instance_init (GTypeInstance * instance,
				      gpointer g_class);

static void __layout_table (MiauiLandmarkTable * self);
static void __refresh_list_internal (MiauiLandmarkTable * self);
static void __add_elements (const gpointer key, MiaLandmark * lm,
			    GtkTreeStore * ts);

GType
miaui_landmark_table_get_type (void)
{
	static GType type = 0;
	if (type == 0) {
		static const GTypeInfo info = {
			sizeof (MiauiLandmarkTableClass),
			NULL,	/* base_init */
			NULL,	/* base_finalize */
			__miaui_landmark_table_class_init,	/* class_init */
			NULL,	/* class_finalize */
			NULL,	/* class_data */
			sizeof (MiauiLandmarkTable),
			0,	/* n_preallocs */
			__miaui_landmark_table_instance_init,	/* instance_init */
			NULL
		};
		type = g_type_register_static (GTK_TYPE_VBOX,
					       "MiauiLandmarkTableType",
					       &info, 0);
	}
	return type;
}

MiauiLandmarkTable *
miaui_landmark_table_new (void)
{
	MiauiLandmarkTable *obj =
		(MiauiLandmarkTable *)
		g_object_new (MIAUI_TYPE_LANDMARK_TABLE, NULL);
	/* add your code here */
	
	return obj;
}

void 
miaui_landmark_table_set_landmarks_visible(MiauiLandmarkTable *self, gboolean visible)
{
	g_assert(MIAUI_IS_LANDMARK_TABLE(self));
	if (MIA_IS_GLDRAWABLE(self->gl_landmarks))
		MIA_GLDRAWABLE(self->gl_landmarks)->is_visible = visible;
}

void miaui_landmark_table_set_landmarklist(MiauiLandmarkTable *self, 
					   MiaLandmarklist *lml)
{
	GValue value = G_VALUE_INIT;
	MiauiLandmarkTableClass *klass; 
	g_assert(MIAUI_IS_LANDMARK_TABLE(self));
	if (lml)
		g_assert(MIA_IS_LANDMARKLIST(lml));
	
	klass = MIAUI_LANDMARK_TABLE_GET_CLASS(self);
	g_assert(klass);
	g_value_init(&value, G_TYPE_POINTER);
	g_value_set_pointer(&value, lml);
	
	g_object_set_property(G_OBJECT(self->gl_landmarks), 
	                      mia_gllandmark_prop_list, &value);
	
	if (self->lml)
		g_object_unref(self->lml);
	else 
		g_signal_emit(self, klass->lmlist_create_id, 0, 
	                      self->gl_landmarks, NULL);
	self->lml = lml; 
	__refresh_list_internal (self);
	
	self->dirty = FALSE; 
	
	g_signal_emit(self, klass->lmlist_change_id, 
		      0, NULL);
}

gboolean miaui_landmark_table_is_dirty(MiauiLandmarkTable *self)	
{
	g_assert(MIAUI_IS_LANDMARK_TABLE (self));
	return self->dirty; 
}	

void miaui_landmark_table_set_listname(MiauiLandmarkTable *self, const gchar *name)
{
	g_assert(MIAUI_IS_LANDMARK_TABLE (self));
	
	if (name) {
		gtk_label_set_text(self->file_label, name);
		if (self->lml_filename)
			g_string_assign(self->lml_filename, name);
		else	
			self->lml_filename = g_string_new(name);
	} else {
		gtk_label_set_text(self->file_label,  _("Landmarkset: (unknown)"));
		if (self->lml_filename)	{
			g_string_free(self->lml_filename, TRUE);
			self->lml_filename = NULL; 
		}	
	}
}

void 
miaui_landmark_table_for_each_landmark(MiauiLandmarkTable *self, GHFunc func)
{
	g_assert(MIAUI_IS_LANDMARK_TABLE (self));
	
	const gchar *name = mia_landmarklist_get_selected(self->lml);
	mia_landmarklist_foreach(self->lml, func, self->lml);
	mia_landmarklist_set_selected(self->lml, name);
}

const gchar *
miaui_landmark_table_get_name(MiauiLandmarkTable *self)
{
	g_assert(MIAUI_IS_LANDMARK_TABLE (self));
	return gtk_entry_get_text(self->name_entry);
}


void 
miaui_landmark_table_set_name(MiauiLandmarkTable *self, const gchar *name)
{
	g_assert(MIAUI_IS_LANDMARK_TABLE (self));
	
	gtk_entry_set_text(self->name_entry, name); 
	self->dirty = TRUE; 
	
}

gboolean
miaui_landmark_table_use_camera(MiauiLandmarkTable *self)
{
	g_assert(MIAUI_IS_LANDMARK_TABLE (self));
	return 
		gtk_toggle_button_get_active(self->cameramode);
}


gboolean
miaui_landmark_table_use_isovalue(MiauiLandmarkTable *self)
{
	g_assert(MIAUI_IS_LANDMARK_TABLE (self));
	return 
		gtk_toggle_button_get_active(self->isomode);
}


static void
__miaui_landmark_table_set_property (GObject * object,
				     guint property_id,
				     const GValue * UNUSED(value), GParamSpec * pspec)
{
	switch (property_id){
		/* handle your properties here */
	default:
		/* We don't have any other property... */
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id,
						   pspec);
	}
}

static void
__miaui_landmark_table_get_property (GObject * object,
				     guint property_id,
				     GValue * UNUSED(value), GParamSpec * pspec)
{
	switch (property_id){
		/* handle your properties here */
	default:
		/* We don't have any other property... */
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id,
						   pspec);
	}
}

static gboolean
__do_save (const gchar * filename, MiauiLandmarkTable * self)
{
	if (!self->lml)
		return TRUE;
	
	mia_landmarklist_set_name(self->lml, 
				  gtk_entry_get_text(self->name_entry));
	if (!mia_landmarklist_save (self->lml, filename)){
		global_show_error_message (_("unable to save %s: %s"),
					   filename, g_strerror (errno));
		return FALSE;
	}
	self->dirty = FALSE; 
	return TRUE;
}

static void 
__set_filename_label(GtkLabel *label, const gchar *fname)
{
	gchar buf[1024];
	gchar *s = strrchr(fname, '/');
	if (s) {
		++s; 
		g_snprintf(buf,sizeof(buf),"Landmarkset: %s", s);
		
	} else
		g_snprintf(buf,sizeof(buf),"Landmarkset: %s", fname);
	gtk_label_set_text(label, buf);
}

static void
__save_set_as (GtkWidget * UNUSED(button), MiauiLandmarkTable * self)
{
	const gchar *filename = gtk_file_selection_get_filename (self->fs);
	if (filename) {
		if (__do_save (filename, self)) {
			self->lml_filename = g_string_new (filename);
			__set_filename_label(self->file_label, filename);
		}
	}
	gtk_widget_destroy (GTK_WIDGET (self->fs));
	gtk_main_quit();
}

static void
__destroy_callback(GtkWidget *self, GtkWidget *UNUSED(other)) 
{
	gtk_widget_destroy (self);
	gtk_main_quit();	
}

static void
__on_save_as_set_clicked (GtkWidget * UNUSED(button), MiauiLandmarkTable * self)
{
	/* \todo disable save buttons, if no landmarks exist*/
	if (!self->lml)
		return; 
	self->fs =
		GTK_FILE_SELECTION (gtk_file_selection_new
				    (_("Save landmark set as ...")));
	gtk_file_selection_complete (self->fs, "*.lmx");
	g_signal_connect (self->fs->ok_button, "clicked",
			  G_CALLBACK (__save_set_as), self);
	g_signal_connect_swapped (self->fs->cancel_button, "clicked",
				  G_CALLBACK (__destroy_callback), self->fs);
	gtk_widget_show (GTK_WIDGET (self->fs));
	gtk_main ();
}

void miaui_landmark_table_save_as_handler(MiauiLandmarkTable * self)
{
	g_assert(MIAUI_LANDMARK_TABLE(self));
	__on_save_as_set_clicked (NULL, self);
}


static void
__on_save_set_clicked (GtkWidget * button, MiauiLandmarkTable * self)
{
	if (self->lml_filename)
		__do_save (self->lml_filename->str, self);
	else
		__on_save_as_set_clicked (button, self);
}

gboolean 
miaui_landmark_table_save_landmarks(MiauiLandmarkTable *self)
{
	g_assert(MIAUI_IS_LANDMARK_TABLE(self));
	__on_save_set_clicked(NULL, self);
	return !self->dirty; 
}

static void
__do_open_set (GtkWidget * UNUSED(button), MiauiLandmarkTable * self)
{
	MiaLandmarklist *lml;
	const gchar *filename = gtk_file_selection_get_filename (self->fs);

	lml = mia_landmarklist_new_from_file (filename);
	if (lml){
		miaui_landmark_table_set_landmarklist(self, lml);
		
		if (self->lml_filename)
			self->lml_filename =
				g_string_assign (self->lml_filename,
						 filename);
		else
			self->lml_filename = g_string_new (filename);
		/* redraw the list */
		gtk_entry_set_text(self->name_entry, 
				   mia_landmarklist_get_name(self->lml));
		
		__set_filename_label(self->file_label, filename);
		
		gtk_widget_destroy (GTK_WIDGET (self->fs));
	}else{
		gtk_file_selection_complete (self->fs, filename);
	}
	/*gtk_widget_destroy(GTK_WIDGET(self->fs));*/
}

static void
__on_open_set_clicked (GtkWidget * UNUSED(button), MiauiLandmarkTable * self)
{
	self->fs =
		GTK_FILE_SELECTION (gtk_file_selection_new
				    (_("Open landmark set ...")));
	
	gtk_file_selection_complete (self->fs, "*.lmx");
	
	g_signal_connect (self->fs->ok_button, "clicked",
			  G_CALLBACK (__do_open_set), self);

	g_signal_connect_swapped (self->fs->cancel_button, "clicked",
				  G_CALLBACK (gtk_widget_destroy), self->fs);
	gtk_widget_show (GTK_WIDGET (self->fs));
}


static void 
__add_menuitem(GtkMenu *menu, gchar *label, gpointer callback, 
                      gpointer data) 
{
	GtkWidget *child = gtk_menu_item_new_with_label (label);
	g_signal_connect (child, "activate",
			  G_CALLBACK (callback), data);
	gtk_widget_show (child);
	gtk_menu_append (menu, child);
}

void
miaui_landmark_table_add_landmark(MiauiLandmarkTable *self, MiaLandmark * lm)
{
	g_assert (self);
	g_assert (lm);

	GtkTreeStore *ts =
		GTK_TREE_STORE (gtk_tree_view_get_model (self->tv));
	const gchar *lmname = mia_landmark_get_name (lm);
	
	if (!self->lml) {
		MiaLandmarklist *lml = mia_landmarklist_new ("(unnamed)");
		miaui_landmark_table_set_landmarklist(self, lml);
	}

	if (!mia_landmarklist_insert (self->lml, lm)) {
		global_show_error_message(_("Landmark name already in use: %s"),
						  mia_landmark_get_name(lm));
		return; 
	}
	__add_elements ((const gpointer) lmname, lm, ts);
	self->dirty = TRUE; 
}

MiaLandmark *
miaui_landmark_table_set_landmark(MiauiLandmarkTable * self, 
				  MiaVector3d *loc, 
				  MiaCamera *camera, 
                                  gfloat iso)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	MiaLandmark *lm;
	gchar *name; 
	
	g_assert(MIAUI_IS_LANDMARK_TABLE(self));
	if (!loc)
		return NULL; 
		
	if (!gtk_tree_selection_get_selected (self->selection, &model, &iter)) {
		global_show_error_message("Must select a landmark first.");
		return  NULL; 
	}
		
	gtk_tree_model_get (model, &iter, COL_NAME, &name, -1);
	lm = mia_landmarklist_get_landmark (self->lml, name);
	g_free (name);
	
	/* Editing an existing Landmark destroys something */
	g_assert(lm);
	
	mia_landmark_set_camera(lm, mia_camera_dup(camera));
	mia_landmark_set_iso_value(lm, iso);
	/* take over this location */
	g_object_ref(loc);
	mia_landmark_set_location(lm, loc);
	
	gtk_tree_store_set (GTK_TREE_STORE(model), &iter, COL_LOCATION, 
			    mia_landmark_get_location(lm), -1);
	
	return lm; 
}

void 
miaui_landmark_table_auto_advance(MiauiLandmarkTable *self)
{
	/* count up iter, to select new landmark__mia_landmarklist_finalize;*/
	GtkTreeModel *model;
	GtkTreeIter iter;
	
	if (gtk_toggle_button_get_active(self->advancemode))
		if (gtk_tree_selection_get_selected (self->selection, &model, &iter))
			if (gtk_tree_model_iter_next(model, &iter)) {
				gtk_tree_selection_select_iter  (self->selection, & iter);
				gtk_tree_view_scroll_to_cell (self->tv,
                                             gtk_tree_model_get_path(model, &iter),
                                             NULL, TRUE, 0.0, 0.0);
			}
}

static void
__do_add_lm (const gchar * lmname, MiaLandmark *dummy, MiauiLandmarkTable *lm_table, GtkWidget *UNUSED(w))
{
	MiaLandmark *lm = mia_landmark_new(lmname, NULL, 1.0, NULL, NULL);
	g_assert(!dummy);
	miaui_landmark_table_add_landmark(lm_table, lm);
}

static void
__on_add_lm_clicked (GtkWidget * UNUSED(button), MiauiLandmarkTable * self)
{
	MiauiLandmarkInput *lmi =
		miaui_landmark_input_new (_("Add new landmark"), NULL);

	miaui_landmark_input_set_add_callback (lmi, __do_add_lm, self, NULL);

	gtk_widget_show (GTK_WIDGET (lmi));
}

void
__do_edit_lm (const gchar * lmname, MiaLandmark *lm, MiauiLandmarkTable *self, GtkWidget *UNUSED(w))
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	gchar *name; 

	
	g_assert (self);
	g_assert (lm);
	gtk_tree_selection_get_selected (self->selection, &model, &iter);
	gtk_tree_model_get (model, &iter, COL_NAME, &name, -1);
	gtk_tree_store_remove (GTK_TREE_STORE (model), &iter);
	
	g_object_ref(lm);
	mia_landmarklist_delete_landmark(self->lml, 
					 mia_landmark_get_name(lm));
	mia_landmark_set_name(lm, lmname);
	mia_landmarklist_insert(self->lml, lm);
	
	__add_elements ((const gpointer) lmname, lm, GTK_TREE_STORE (model));
	
	self->dirty = TRUE; 
}

static void
__edit_lm (MiaLandmark *lm, MiauiLandmarkTable * self)
{
	MiauiLandmarkInput *lmi;
	
	lmi = miaui_landmark_input_new (_("Edit landmark"), lm);
	miaui_landmark_input_set_add_callback (lmi, __do_edit_lm, self, NULL);
	gtk_widget_show (GTK_WIDGET (lmi));
}

MiaLandmark *
miaui_landmark_table_get_selected_landmark(MiauiLandmarkTable *self)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	MiaLandmark *lm = NULL; 
	
	if (gtk_tree_selection_get_selected (self->selection, &model, &iter)) {
		gchar *name; 
		gtk_tree_model_get (model, &iter, COL_NAME, &name, -1);
		lm = mia_landmarklist_get_landmark (self->lml, name);
		g_free (name);
	}
	return lm; 
}

static void
__on_edit_lm_clicked (GtkWidget * UNUSED(button), MiauiLandmarkTable * self)
{
	MiaLandmark *lm = miaui_landmark_table_get_selected_landmark(self);
	if (lm) 
		__edit_lm (lm, self);
}

static void
__on_tvrow_activated (GtkTreeView * UNUSED(treeview),
		      GtkTreePath * UNUSED(path),
		      GtkTreeViewColumn * UNUSED(col), MiauiLandmarkTable * self)
{
	MiaLandmark *lm = miaui_landmark_table_get_selected_landmark(self) ;
	if (lm) 
		__edit_lm (lm, self);
}

static void
__on_del_lm_clicked (GtkWidget * UNUSED(button), MiauiLandmarkTable * self)
{
	GtkTreeModel *model;
	GtkTreeIter iter;

	gchar *name;
	if (gtk_tree_selection_get_selected (self->selection, &model, &iter)){
		gtk_tree_model_get (model, &iter, COL_NAME, &name, -1);
		gtk_tree_store_remove (GTK_TREE_STORE (model), &iter);
		mia_landmarklist_delete_landmark (self->lml, name);
		g_free (name);
		self->dirty = TRUE; 
	}
}

void __set_picfile(MiauiLandmarkTable * self, MiaLandmark *lm)	
{
	const gchar *picfile = mia_landmark_get_picfile(lm);
	if (picfile && strlen(picfile)) {
		GError *error = NULL; 
		GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file(picfile,&error);
		if (error) {
			g_warning(_("failed loading %s: %s"), picfile, error->message); 
			g_error_free(error);
			gtk_widget_set_size_request(GTK_WIDGET(self->template),256, 0);
		}else {
			gtk_image_set_from_pixbuf(self->template, pixbuf);
			gtk_widget_set_size_request(GTK_WIDGET(self->template),256, 256);
		}
	}else
		gtk_widget_set_size_request(GTK_WIDGET(self->template),256, 0);
}

gboolean
__selection_func(GtkTreeSelection *UNUSED(selection),
		 GtkTreeModel *model,
                 GtkTreePath *path,
                 gboolean path_currently_selected,
                 gpointer data)
{
	GtkTreeIter iter; 
	MiauiLandmarkTable *self = MIAUI_LANDMARK_TABLE(data);
	
	if (path_currently_selected)
		return TRUE; 
	
	if (gtk_tree_model_get_iter(model, &iter, path)) {
		gchar *name; 
		MiaLandmark *lm; 
		MiauiLandmarkTableClass *klass;
		gtk_tree_model_get (model, &iter, COL_NAME, &name, -1);
		
		lm = mia_landmarklist_get_landmark(self->lml, name);
		
		if (lm) {
			klass = MIAUI_LANDMARK_TABLE_GET_CLASS(self);
			
			mia_landmarklist_set_selected(self->lml, name);
		
			__set_picfile(self, lm);	
			/* now set the image*/
			g_signal_emit(self, klass->selection_changed_id, 0, 
				      lm,	
				      NULL);
			
		}
		g_free(name);
		
	}
	return TRUE; 
}



static void 
__miaui_landmark_clear_locations(GtkButton *UNUSED(button), gpointer user_data)
{
	/* Probably ask the user ... */
	MiauiLandmarkTable *self = MIAUI_LANDMARK_TABLE(user_data);
	if (self->lml)
		mia_landmarklist_clear_locations(self->lml);
	
	__refresh_list_internal(self); 
	
	gtk_widget_queue_draw(glview);
	
	gtk_widget_queue_draw(GTK_WIDGET(self));
	self->dirty = TRUE; 	
}

static void 
__on_clear_lm_clicked(GtkWidget *UNUSED(w), gpointer user_data) 
{
	MiauiLandmarkTable *self = MIAUI_LANDMARK_TABLE(user_data);
	MiaLandmark *lm = miaui_landmark_table_get_selected_landmark(self);
	if (lm) {
		mia_landmark_set_location(lm, NULL);
		__refresh_list_internal(self); 
		gtk_widget_queue_draw(glview);
		gtk_widget_queue_draw(GTK_WIDGET(self));
		self->dirty = TRUE; 
	}
}

static void
__create_landmark_menu (MiauiLandmarkTable * self)
{
	self->landmark_menu = gtk_menu_new ();
	
	__add_menuitem(GTK_MENU(self->landmark_menu), _("Edit Landmark"), 
		       __on_edit_lm_clicked , self);
	
	__add_menuitem(GTK_MENU(self->landmark_menu), _("Add Landmark"), 
		       __on_add_lm_clicked, self);
	
	__add_menuitem(GTK_MENU(self->landmark_menu), _("Clear Landmark"), 
		       __on_clear_lm_clicked, self);
	
	__add_menuitem(GTK_MENU(self->landmark_menu), _("Delete Landmark"), 
		       __on_del_lm_clicked, self);
}

void
__view_popup_menu (GtkWidget *UNUSED(treeview), GdkEventButton *event, gpointer userdata)
{
	MiauiLandmarkTable *self = MIAUI_LANDMARK_TABLE(userdata);
	g_assert(self);
	
	gtk_menu_popup(GTK_MENU(self->landmark_menu), NULL, NULL, NULL, NULL,
                   (event != NULL) ? event->button : 0,
                   gdk_event_get_time((GdkEvent*)event));
}	

gboolean
__on_button_pressed (GtkWidget *treeview, GdkEventButton *event, gpointer userdata)
{
    /* single click with the right mouse button? */
	if (event->type == GDK_BUTTON_PRESS  &&  event->button == 3)  {
		GtkTreeSelection *selection;
		selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
		if (gtk_tree_selection_count_selected_rows(selection)  <= 1){
			GtkTreePath *path;

			   /* Get tree path for row that was clicked */
			if (gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(treeview),
                                             event->x, event->y,
                                             &path, NULL, NULL, NULL)) {
				gtk_tree_selection_unselect_all(selection);
				gtk_tree_selection_select_path(selection, path);
				gtk_tree_path_free(path);
			}
		}
		__view_popup_menu(treeview, event, userdata);
		return TRUE; /* we handled this */	
      } 
      return FALSE; /* we did not handle this */
}

gboolean
__on_popup_menu (GtkWidget *treeview, gpointer userdata)
{
	__view_popup_menu(treeview, NULL, userdata);
	return TRUE; /* we handled this */
}

static void
__miaui_landmark_table_instance_init (GTypeInstance * instance,
				      gpointer UNUSED(g_class))
{
	GtkWidget *button;
	GtkWidget *bruce;
	GtkWidget *frame;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *image;
	GtkTable *table; 

	MiauiLandmarkTable *self = (MiauiLandmarkTable *) instance;
	self->dispose_has_run = FALSE;

	gtk_box_set_homogeneous (GTK_BOX (self), FALSE);

	/* create the widget contens */
	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (self), frame, FALSE, TRUE, 3);

	self->file_label  = GTK_LABEL(gtk_label_new (_("Landmarkset: (unknown)")));
	gtk_widget_show (GTK_WIDGET(self->file_label));
	gtk_frame_set_label_widget (GTK_FRAME (frame), GTK_WIDGET(self->file_label));

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (frame), vbox);

	hbox = gtk_hbox_new (TRUE, 0);
	gtk_widget_show (hbox);
	gtk_box_set_homogeneous (GTK_BOX (hbox), FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	button = gtk_button_new_from_stock ("gtk-open");
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	g_signal_connect (button, "clicked",
			  G_CALLBACK (__on_open_set_clicked), self);

	button = gtk_button_new ();
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 0);
	g_signal_connect (button, "clicked",
			  G_CALLBACK (__on_save_set_clicked), self);

	image = gtk_image_new_from_stock ("gtk-save", GTK_ICON_SIZE_BUTTON);
	gtk_widget_show (image);
	gtk_container_add (GTK_CONTAINER (button), image);


	button = gtk_button_new_from_stock ("gtk-save-as");
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
	gtk_widget_show (button);
	g_signal_connect (button, "clicked",
			  G_CALLBACK (__on_save_as_set_clicked), self);
			  
	table = GTK_TABLE(gtk_table_new (2,1, FALSE) );
	gtk_widget_show (GTK_WIDGET(table));
	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET(table), FALSE, FALSE, 0);
	
	bruce = gtk_label_new(_("Source Dataset:"));
	gtk_widget_show (bruce);
	gtk_table_attach_defaults(table, bruce, 0,1,0,1);
                                             
	self->name_entry = GTK_ENTRY(gtk_entry_new());
	gtk_widget_show (GTK_WIDGET(self->name_entry));
	gtk_table_attach_defaults(table, GTK_WIDGET(self->name_entry), 1,2,0,1);
	
	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (self), frame, TRUE, TRUE, 3);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (frame), vbox);

	self->advancemode = GTK_TOGGLE_BUTTON(gtk_check_button_new_with_label (_("Advance automatic")));	  
	gtk_toggle_button_set_active(self->advancemode, TRUE); 
	gtk_widget_show (GTK_WIDGET(self->advancemode));
	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET(self->advancemode), FALSE, FALSE, 0);

	self->cameramode = GTK_TOGGLE_BUTTON(gtk_check_button_new_with_label (_("Use landmark view info")));	  
	gtk_toggle_button_set_active(self->cameramode , TRUE); 
	gtk_widget_show (GTK_WIDGET(self->cameramode ));
	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET(self->cameramode), FALSE, FALSE, 0);
	
	self->isomode = GTK_TOGGLE_BUTTON(gtk_check_button_new_with_label (_("Use landmark iso info")));	  
	gtk_toggle_button_set_active(self->isomode  , TRUE); 
	gtk_widget_show (GTK_WIDGET(self->isomode  ));
	gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET(self->isomode), FALSE, FALSE, 0);
	
	button = gtk_button_new_with_label(_("Clear all Landmark Locations"));
	g_signal_connect(button, "clicked", G_CALLBACK(__miaui_landmark_clear_locations), self);
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, TRUE, 0);

	bruce = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (bruce);
	gtk_box_pack_start (GTK_BOX (vbox), bruce, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (bruce),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);

	self->tv = GTK_TREE_VIEW (gtk_tree_view_new ());
	self->selection = gtk_tree_view_get_selection (self->tv);
	gtk_tree_selection_set_mode (self->selection, GTK_SELECTION_SINGLE);
	
	gtk_tree_selection_set_select_function(self->selection, __selection_func, 
					       self, NULL);

	gtk_widget_show (GTK_WIDGET (self->tv));
	gtk_container_add (GTK_CONTAINER (bruce), GTK_WIDGET (self->tv));

	g_signal_connect (self->tv, "row-activated",
			  G_CALLBACK (__on_tvrow_activated), self);

	if (0) {
	hbox = gtk_hbox_new (TRUE, 0);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	button = gtk_button_new_from_stock ("gtk-add");
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	g_signal_connect (button, "clicked", G_CALLBACK (__on_add_lm_clicked),
			  self);


	button = gtk_button_new_with_mnemonic ("Edit");
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	g_signal_connect (button, "clicked",
			  G_CALLBACK (__on_edit_lm_clicked), self);

	button = gtk_button_new_from_stock ("gtk-delete");
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);

	g_signal_connect (button, "clicked", G_CALLBACK (__on_del_lm_clicked),
			  self);
	}// end if (0)
	
	g_signal_connect(self->tv, "button-press-event", (GCallback) __on_button_pressed, self);
	g_signal_connect(self->tv, "popup-menu", (GCallback) __on_popup_menu, self);
	
	self->template = GTK_IMAGE(gtk_image_new());
	gtk_widget_set_size_request(GTK_WIDGET(self->template),256, 0);
	gtk_widget_show (GTK_WIDGET(self->template));
	
	g_object_ref (G_OBJECT (self->template));
	gtk_object_sink (GTK_OBJECT (self->template));
	
	gtk_box_pack_start (GTK_BOX (self), GTK_WIDGET(self->template), FALSE, TRUE, 0);
	
	__layout_table (self);
	__refresh_list_internal (self);

	self->gl_landmarks = mia_gllandmark_new("Landmarks", self->lml);
	
	self->lml = NULL; 
	
	self->dirty = FALSE; 
	
	gtk_widget_set_size_request(GTK_WIDGET (self), 320,-1);
	gtk_widget_show (GTK_WIDGET (self));
	
	__create_landmark_menu(self);
}

void 
miaui_landmark_table_load_handler(MiauiLandmarkTable * self)
{
	__on_open_set_clicked(NULL, self);
}

void 
miaui_landmark_table_clear_locations_handler(MiauiLandmarkTable * self)
{
	__miaui_landmark_clear_locations(NULL, self);
}

void 
miaui_landmark_table_save_handler(MiauiLandmarkTable * self)
{
	__on_save_set_clicked(NULL, self);
}

void 
miaui_landmark_table_add_landmark_handler(MiauiLandmarkTable * self)
{
	__on_add_lm_clicked(NULL, self);
}

void 
miaui_landmark_table_del_landmark_handler(MiauiLandmarkTable * self)
{
	__on_del_lm_clicked(NULL, self);
}

void 
miaui_landmark_table_edit_landmark_handler(MiauiLandmarkTable * self)
{
	__on_edit_lm_clicked(NULL, self);
}

void 
miaui_landmark_table_clear_landmark_handler(MiauiLandmarkTable * self)
{
	__on_clear_lm_clicked(NULL, self);
}

static GObject *
__miaui_landmark_table_constructor (GType type,
				    guint n_construct_properties,
				    GObjectConstructParam *
				    construct_properties)
{
	GObject *obj;
	{
		/* Invoke parent constructor. */
		MiauiLandmarkTableClass *klass;
		GObjectClass *parent_class;
		klass = MIAUI_LANDMARK_TABLE_CLASS (g_type_class_peek
						    (MIAUI_TYPE_LANDMARK_TABLE));
		parent_class =
			G_OBJECT_CLASS (g_type_class_peek_parent (klass));
		obj = parent_class->constructor (type, n_construct_properties,
						 construct_properties);
	}

	/* add your code here */

	return obj;
}

static void
__miaui_landmark_table_dispose (GObject * obj)
{
	MiauiLandmarkTableClass *klass;
	GObjectClass *parent_class;
	MiauiLandmarkTable *self = (MiauiLandmarkTable *) obj;

	if (self->dispose_has_run)
		/* If dispose did already run, return. */
		return;
	/* Make sure dispose does not run twice. */
	self->dispose_has_run = TRUE;

	/* add your code here */
	if (self->gl_landmarks) {
		g_object_unref(self->gl_landmarks); 
		self->gl_landmarks = NULL; 
	}
	
	if (self->lml) {
		g_object_unref (self->lml);
		self->lml = NULL; 
	}

	/* chain up to the parent class */
	klass = MIAUI_LANDMARK_TABLE_CLASS (g_type_class_peek
					    (MIAUI_TYPE_LANDMARK_TABLE));
	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	parent_class->dispose (obj);
}

static void
__miaui_landmark_table_finalize (GObject * obj)
{
	MiauiLandmarkTableClass *klass;
	GObjectClass *parent_class;
	MiauiLandmarkTable *self = (MiauiLandmarkTable *) obj;

	/* add your destruction code here */
	if (self->lml_filename)
		g_string_free (self->lml_filename, TRUE);

	/* chain up to the parent class */
	klass = MIAUI_LANDMARK_TABLE_CLASS (g_type_class_peek
					    (MIAUI_TYPE_LANDMARK_TABLE));
	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	parent_class->finalize (obj);
}

static void
__miaui_landmark_table_class_init (gpointer g_class, gpointer UNUSED(g_class_data))
{
	GType param_types[1];
	MiauiLandmarkTableClass *klass = MIAUI_LANDMARK_TABLE_CLASS(g_class);
	GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);

	gobject_class->set_property = __miaui_landmark_table_set_property;
	gobject_class->get_property = __miaui_landmark_table_get_property;
	gobject_class->dispose = __miaui_landmark_table_dispose;
	gobject_class->finalize = __miaui_landmark_table_finalize;
	gobject_class->constructor = __miaui_landmark_table_constructor;

	/* Create a signal to notify a change of the landmark list */
	
	param_types[0] = G_TYPE_POINTER; 
	
	
	klass->lmlist_create_id = 
		g_signal_newv("lml-create", 
	                      G_TYPE_FROM_CLASS(g_class), 
			      G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS, 
		              NULL, NULL, NULL,
	                      g_cclosure_marshal_VOID__POINTER,
			      G_TYPE_NONE, 
	                      1, 
			      param_types);
			      
	klass->lmlist_change_id = 
		g_signal_newv("lml-change", 
	                      G_TYPE_FROM_CLASS(g_class), 
			      G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS, 
		              NULL, NULL, NULL,
	                      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 
	                      0, 
			      NULL);
			   
	klass->selection_changed_id = 
		g_signal_newv("selection-changed", 
	                      G_TYPE_FROM_CLASS(g_class), 
			      G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS, 
		              NULL, NULL, NULL,
	                      g_cclosure_marshal_VOID__POINTER,
			      G_TYPE_NONE, 
	                      1, 
			      param_types);
	
	/* add your code here (e.g. define properties) */
}

gboolean
miaui_landmark_table_test (void)
{
	gboolean result = FALSE;
	MiauiLandmarkTable *test = miaui_landmark_table_new ();
	g_return_val_if_fail (test, FALSE);
	result = MIAUI_IS_LANDMARK_TABLE (test);

	/* add your tests here */


	g_object_unref (G_OBJECT (test));
	return result;
}

MiauiLandmarkTable *landmarktable = NULL; 

GtkWidget *
miaui_landmark_create (gchar * UNUSED(widget_name), gchar * UNUSED(string1), gchar * UNUSED(string2),
		       gint UNUSED(int1), gint UNUSED(int2))
{
	g_assert(!landmarktable);
	
	landmarktable = miaui_landmark_table_new ();
	return GTK_WIDGET (landmarktable);
}

gint
__sort_function (GtkTreeModel * model, GtkTreeIter * a, GtkTreeIter * b,
		 gpointer UNUSED(data))
{
	gint ret = 0;
	gchar *n1, *n2;

	gtk_tree_model_get (model, a, COL_NAME, &n1, -1);
	gtk_tree_model_get (model, b, COL_NAME, &n2, -1);

	if (n1 == NULL && n2 == NULL) {
		if (n1 != NULL || n2 != NULL)
			ret = (n1 == NULL) ? -1 : 1;
		/* else ret = 0 */
	}
	else{
#ifdef USE_ISO_SORTING
		MiauiLandmarkTable *self = MIAUI_LANDMARK_TABLE(data);
		MiaLandmark *l1, *l2; 
		gfloat iso1, iso2; 
		
		g_assert(self);
		g_assert(self->lml);
		
		l1 = mia_landmarklist_get_landmark(self->lml, n1);
		l2 = mia_landmarklist_get_landmark(self->lml, n2);
		iso1 = mia_landmark_get_iso_value(l1);
		iso2 = mia_landmark_get_iso_value(l2);
		
		if (iso1 != iso2)
			ret = (iso1 < iso2);				
		else
#endif
			ret = g_utf8_collate (n1, n2);	
	}
	
	g_free (n1);
	g_free (n2);
	return ret;
}

static void
__add_elements (const gpointer UNUSED(key), MiaLandmark * lm, GtkTreeStore * ts)
{
	GtkTreeIter iter;
	g_assert (lm);

	gtk_tree_store_append (ts, &iter, NULL);
	gtk_tree_store_set (ts, &iter,
			    COL_NAME, mia_landmark_get_name (lm),
			    COL_LOCATION, mia_landmark_get_location (lm), -1);
}

void
__vector_cell_data_function (GtkTreeViewColumn * UNUSED(col),
			     GtkCellRenderer * renderer,
			     GtkTreeModel * model,
			     GtkTreeIter * iter, gpointer UNUSED(user_data))
{
	MiaVector3d *location;
	gchar buf[40];

	gtk_tree_model_get (model, iter, COL_LOCATION, &location, -1);

	if (MIA_IS_VECTOR3D (location)){
		g_snprintf (buf, sizeof (buf), "< %5.1f %5.1f %5.1f >",
			    location->x, location->y, location->z);
		g_object_set (renderer, "text", buf, NULL);
	}else
		g_object_set (renderer, "text", _("unknown"), NULL);
	
}

static void
__refresh_list_internal (MiauiLandmarkTable * self)
{
	GtkTreeStore *treestore;

	treestore = GTK_TREE_STORE (gtk_tree_view_get_model (self->tv));
	g_object_ref (treestore);

	gtk_tree_view_set_model (self->tv, NULL);
	gtk_tree_store_clear (treestore);

	if (self->lml)
		mia_landmarklist_foreach (self->lml,
					  (GHFunc) __add_elements, treestore);

	gtk_tree_view_set_model (self->tv, GTK_TREE_MODEL (treestore));
	g_object_unref (treestore);
}

static void
__layout_table (MiauiLandmarkTable * self)
{
	GtkTreeStore *treestore;
	GtkTreeViewColumn *tvc;
	GtkCellRenderer *renderer;

	treestore = gtk_tree_store_new (2, G_TYPE_STRING, G_TYPE_POINTER);
	gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (treestore), 0,
					 __sort_function, self, NULL);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (treestore),
					      0, GTK_SORT_ASCENDING);


	tvc = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (tvc, _("Name"));
	gtk_tree_view_append_column (self->tv, tvc);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (tvc, renderer, TRUE);
	gtk_tree_view_column_add_attribute (tvc, renderer, "text", 0);

	tvc = gtk_tree_view_column_new ();
	gtk_tree_view_column_set_title (tvc, _("Location"));
	gtk_tree_view_append_column (self->tv, tvc);

	renderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_column_pack_start (tvc, renderer, TRUE);
	gtk_tree_view_column_add_attribute (tvc, renderer, "text", 0);

	gtk_tree_view_column_set_cell_data_func (tvc, renderer,
						 __vector_cell_data_function,
						 NULL, NULL);


	gtk_tree_view_set_model (self->tv, GTK_TREE_MODEL (treestore));
}

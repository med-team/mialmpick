/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */
/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#ifndef __landmarktable_h
#define __landmarktable_h

#include <gtk/gtk.h>

#include <mialm.h>

#define MIAUI_TYPE_LANDMARK_TABLE (miaui_landmark_table_get_type())
#define MIAUI_LANDMARK_TABLE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIAUI_TYPE_LANDMARK_TABLE, MiauiLandmarkTable))
#define MIAUI_LANDMARK_TABLE_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIAUI_TYPE_LANDMARK_TABLE, MiauiLandmarkTableClass))
#define MIAUI_IS_LANDMARK_TABLE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIAUI_TYPE_LANDMARK_TABLE))
#define MIAUI_IS_LANDMARK_TABLE_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIAUI_TYPE_LANDMARK_TABLE))
#define MIAUI_LANDMARK_TABLE_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst), MIAUI_TYPE_LANDMARK_TABLE, MiauiLandmarkTableClass))

typedef struct _MiauiLandmarkTable MiauiLandmarkTable;
typedef struct _MiauiLandmarkTableClass MiauiLandmarkTableClass;

struct _MiauiLandmarkTableClass {
	GtkVBoxClass parent;
	guint lmlist_change_id;
	guint lmlist_create_id;
	guint selection_changed_id;
};

GType miaui_landmark_table_get_type(void);

MiauiLandmarkTable *miaui_landmark_table_new(void);

void 
miaui_landmark_table_set_name(MiauiLandmarkTable *self, const gchar *name);

gboolean 
miaui_landmark_table_save_landmarks(MiauiLandmarkTable *self);

gboolean 
miaui_landmark_table_is_dirty(MiauiLandmarkTable *self);

const gchar *
miaui_landmark_table_get_name(MiauiLandmarkTable *self);


void
miaui_landmark_table_add_landmark(MiauiLandmarkTable *self, MiaLandmark * lm);

MiaLandmark *
miaui_landmarke_table_get_selected_landmark(MiauiLandmarkTable *self);

MiaLandmark *
miaui_landmark_table_get_selected_landmark(MiauiLandmarkTable *self);

MiaLandmark *
miaui_landmark_table_set_landmark(MiauiLandmarkTable * self, 
				  MiaVector3d *loc, 
				  MiaCamera *camera, 
                                  gfloat iso);

void 
miaui_landmark_table_auto_advance(MiauiLandmarkTable *self);

void 
miaui_landmark_table_set_listname(MiauiLandmarkTable *self, const gchar *name);

void miaui_landmark_table_set_landmarklist(MiauiLandmarkTable *self, 
	MiaLandmarklist *lml);


void 
miaui_landmark_table_set_landmarks_visible(MiauiLandmarkTable *self, gboolean visible);

gboolean
miaui_landmark_table_use_camera(MiauiLandmarkTable *self);


gboolean
miaui_landmark_table_use_isovalue(MiauiLandmarkTable *self);

void 
miaui_landmark_table_save_as_handler(MiauiLandmarkTable * self);

void 
miaui_landmark_table_load_handler(MiauiLandmarkTable * self);

void 
miaui_landmark_table_clear_locations_handler(MiauiLandmarkTable * self);

void 
miaui_landmark_table_save_handler(MiauiLandmarkTable * self);

void 
miaui_landmark_table_add_landmark_handler(MiauiLandmarkTable * self);

void 
miaui_landmark_table_del_landmark_handler(MiauiLandmarkTable * self);

void 
miaui_landmark_table_edit_landmark_handler(MiauiLandmarkTable * self);

void 
miaui_landmark_table_clear_landmark_handler(MiauiLandmarkTable * self);

void 
miaui_landmark_table_for_each_landmark(MiauiLandmarkTable *self, GHFunc func);

gboolean miaui_landmark_table_test(void);

#endif

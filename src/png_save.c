/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "png_save.h"
#include <stdio.h>


int png_save(char *name,int w, int h, int bit_depth, int color_type, unsigned char *buffer,int line_stride,int top_down)
{
	FILE *outf; 
	png_structp png_ptr; 
	png_infop png_info;
	int i; 
	
	outf = fopen(name,"wb");
	if (!outf) {
		perror(name);
		return 0;
	}
	
	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
					  (png_voidp)NULL,NULL,NULL);
	if (!png_ptr) {
		fclose(outf);
		fprintf(stderr,"TPNGSaver:unable to create png write struct\n");
		return 0; 
	}
	
	png_info = png_create_info_struct(png_ptr);
	
	if (!png_info) {
		png_destroy_write_struct(&png_ptr,(png_infopp)NULL);
		fclose(outf);
		fprintf(stderr,"TPNGSaver:unable to create png info struct\n");
		return 0; 
	}
	
	if (setjmp(png_jmpbuf(png_ptr))) {
		png_destroy_write_struct(&png_ptr,&png_info);
		fclose(outf);
		return 0; 
	}
	
	
	png_init_io(png_ptr,outf);
	
	png_set_IHDR(png_ptr,png_info,
		     w,h,bit_depth,color_type,
		     PNG_INTERLACE_NONE,
		     PNG_COMPRESSION_TYPE_DEFAULT,
		     PNG_FILTER_TYPE_DEFAULT);
	
	png_write_info(png_ptr,png_info);
	
	if (top_down)
		
		for (i = 0; i < h; i++,buffer+=line_stride) {
			png_write_rows(png_ptr,&buffer,1);
		}
	else {
		for (i = h-1; i >= 0; i--) {
			unsigned char *b = &buffer[i * line_stride];
			png_write_rows(png_ptr,&b,1);
		}
		
	}
	    
	png_write_end(png_ptr,png_info);
	
	png_destroy_write_struct(&png_ptr,&png_info);
	fclose(outf);
	return 1; 
}

int png_save_grayscale(char *name,int w, int h,unsigned char *buffer,int top_down) 
{
	return png_save(name,w,h,8,PNG_COLOR_TYPE_GRAY,buffer,w,top_down);
}

int png_save_rgb(char *name,int w, int h,unsigned char *buffer,int top_down) 
{
	return png_save(name,w,h,8,PNG_COLOR_TYPE_RGB,buffer,3*w,top_down);
}

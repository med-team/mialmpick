/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

 
#ifndef __glview_h
#define __glview_h

#include <gtk/gtk.h>
#include <mialm/miacamera.h>

#define MIAUI_TYPE_GLVIEW (miaui_glview_get_type())
#define MIAUI_GLVIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIAUI_TYPE_GLVIEW, MiauiGLView))
#define MIAUI_GLVIEW_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIAUI_TYPE_GLVIEW, MiauiGLViewClass))
#define MIAUI_IS_GLVIEW(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIAUI_TYPE_GLVIEW))
#define MIAUI_IS_GLVIEW_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIAUI_TYPE_GLVIEW))
#define MIAUI_GLVIEW_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst), MIAUI_TYPE_GLVIEW, MiauiGLViewClass))

typedef struct _MiauiGLView MiauiGLView;
typedef struct _MiauiGLViewClass MiauiGLViewClass;


struct _MiauiGLViewClass {
	GtkDrawingAreaClass parent;
	guint landmark_picked_id;
};

typedef void (*SetLandmarkCallback)(MiaVector3d *loc, MiaCamera *camera);

typedef enum _MiauiGLViewMouseAction MiauiGLViewMouseAction;
enum _MiauiGLViewMouseAction  {
	miaui_glview_rotate = 0,
	miaui_glview_shift, 
	miaui_glview_zoom
};

typedef enum _OptimalDirection OptimalDirection;
enum _OptimalDirection {
	od_xm = 0, 
	od_xp,  
	od_ym, 
	od_yp, 
	od_zm, 
	od_zp, 
	od_unknown
};

G_BEGIN_DECLS

GType 
miaui_glview_get_type(void);

GtkWidget *
miaui_glview_new(void);

void 
miaui_glview_set_mouse_action(MiauiGLView *self, MiauiGLViewMouseAction action);

MiaCamera *
miaui_glview_get_camera(MiauiGLView *self);

void 
miaui_glview_set_camera(MiauiGLView *self, MiaCamera *camera);

void
miaui_glview_reset_camera (MiauiGLView * self);

void 
miaui_glview_update_drawable(MiauiGLView *self, gpointer drawable);

void 
miaui_glview_add_drawable(MiauiGLView *self, gpointer drawable);

gdouble *
miaui_glview_get_back_color_ptr(MiauiGLView * self);

void 
miaui_glview_del_drawable(MiauiGLView *self, gpointer key);

OptimalDirection
miaui_glview_get_draw_direction(MiauiGLView *self); 

MiaVector3d *
miaui_glview_get_light_direction(MiauiGLView *self, MiaVector3d *result);



void 
miaui_glview_set_rotation_center(MiauiGLView *self, MiaVector3d *center);


void 
miaui_glview_enable_zoomin(MiauiGLView *self);
	
void
miaui_glview_disable_zoomin(MiauiGLView *self);

gboolean
miaui_glview_snapshot (MiauiGLView * self, gchar *filename);

void miaui_glview_kill_gl(MiauiGLView * self);

gboolean 
miaui_glview_check_abort_rendering(MiauiGLView *self);

void
glview_lock ();

void
glview_unlock ();

G_END_DECLS

#endif

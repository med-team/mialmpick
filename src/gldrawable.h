/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#ifndef __gldrawable_h
#define __gldrawable_h

#include <glib.h>
#include <glib-object.h>

#include "glview.h"

typedef union {
	guint32 color; 
	struct {
		guint8 r,g,b,a;
	}component;
} MiaColor;	
	
extern const gchar mia_gldrawable_name_property[];

#define MIA_TYPE_GLDRAWABLE (mia_gldrawable_get_type())
#define MIA_GLDRAWABLE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIA_TYPE_GLDRAWABLE, MiaGLDrawable))
#define MIA_GLDRAWABLE_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIA_TYPE_GLDRAWABLE, MiaGLDrawableClass))
#define MIA_IS_GLDRAWABLE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIA_TYPE_GLDRAWABLE))
#define MIA_IS_GLDRAWABLE_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIA_TYPE_GLDRAWABLE))
#define MIA_GLDRAWABLE_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst), MIA_TYPE_GLDRAWABLE, MiaGLDrawableClass))

typedef struct _MiaGLDrawable MiaGLDrawable;
typedef struct _MiaGLDrawableClass MiaGLDrawableClass;

struct _MiaGLDrawable {
	GObject parent;
	gboolean dispose_has_run;
	/* add your class instance elements here */
	gboolean is_attached; 
	
	/* properties of the drawable */
	GString *name; 
	gboolean is_visible;
	gboolean is_transparent; 
	
};

struct _MiaGLDrawableClass {
	GObjectClass parent;
	void (*gl_attach)(MiaGLDrawable *self);
	void (*gl_detach)(MiaGLDrawable *self);
	void (*gl_draw)(MiaGLDrawable *self, MiauiGLView *view, gboolean high_resolution);
	void (*initialize)(MiaGLDrawable *self);
};



GType 
mia_gldrawable_get_type(void);

MiaGLDrawable *
mia_gldrawable_new(gchar *name);


void
mia_gldrawable_gl_initialize(MiaGLDrawable *self);
     
void 
mia_gldrawable_gl_attach(MiaGLDrawable *self);

void 
mia_gldrawable_gl_detach(MiaGLDrawable *self);

void 
mia_gldrawable_gl_draw(MiaGLDrawable *self, MiauiGLView *view, gboolean high_resolution);

const gchar *
mia_gldrawable_get_name(MiaGLDrawable *self);

#define mia_gldrawable_is_transparent(self) self->is_transparent


gboolean mia_gldrawable_test(void);

#endif

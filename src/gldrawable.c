/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "gldrawable.h"



const gchar mia_gldrawable_name_property[] = "name";


static GObject *
__mia_gldrawable_constructor (GType                  type,
		    guint                  n_construct_properties,
		    GObjectConstructParam *construct_properties);


static void 
__mia_gldrawable_class_init (gpointer g_class,
                   gpointer g_class_data);

static void 
__mia_gldrawable_instance_init (GTypeInstance   *instance,
                      gpointer         g_class);

GType mia_gldrawable_get_type(void)
{
	static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MiaGLDrawableClass),
                        NULL,   /* base_init */
                        NULL,   /* base_finalize */
                        __mia_gldrawable_class_init,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        sizeof (MiaGLDrawable),
                        0,      /* n_preallocs */
                        __mia_gldrawable_instance_init,    /* instance_init */
			NULL
                };
                type = g_type_register_static (G_TYPE_OBJECT,
                                               "MiaGLDrawableType",
                                               &info, 0);
        }
        return type;	
}




MiaGLDrawable *mia_gldrawable_new(gchar *name)
{
	MiaGLDrawable *obj = (MiaGLDrawable *)
			g_object_new(MIA_TYPE_GLDRAWABLE,
			             mia_gldrawable_name_property, name, 
				     NULL);
	/* add your code here */ 
		
	return obj; 
}

enum {
	prop_name = 1,
};

static void
__mia_gldrawable_set_property (GObject      *object,
                     guint         property_id,
                     const GValue *value,
                     GParamSpec   *pspec)
{
        MiaGLDrawable *self = (MiaGLDrawable *) object;
	 
        switch (property_id) {
	/* handle your properties here */	
	case prop_name: 
		g_string_assign(self->name, g_value_get_string(value));
		break; 
	default:
                /* We don't have any other property... */
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
__mia_gldrawable_get_property (GObject      *object,
                     guint         property_id,
                     GValue       *value,
                     GParamSpec   *pspec)
{
        MiaGLDrawable *self = (MiaGLDrawable *) object;
	 
        switch (property_id) {
	case prop_name: 
		g_value_set_string(value, self->name->str);
		break; 
        /* handle your properties here */
        default:
                /* We don't have any other property... */
                G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
}

static void
__mia_gldrawable_instance_init (GTypeInstance   *instance,
				gpointer         UNUSED(g_class))
{
        MiaGLDrawable *self = (MiaGLDrawable *)instance;
        self->dispose_has_run = FALSE;
	self->is_attached = FALSE; 
	self->is_visible = TRUE; 
	self->is_transparent = FALSE; 
	self->name = g_string_new(""); 
}

static GObject *
__mia_gldrawable_constructor (GType                  type,
                    guint                  n_construct_properties,
                    GObjectConstructParam *construct_properties)
{
        GObject *obj;
        {
                /* Invoke parent constructor. */
                MiaGLDrawableClass *klass;
                GObjectClass *parent_class;  
                klass = MIA_GLDRAWABLE_CLASS (g_type_class_peek (MIA_TYPE_GLDRAWABLE));
                parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
                obj = parent_class->constructor (type,
                                                 n_construct_properties,
                                                 construct_properties);
        }
        
        /* add your code here */
	
        return obj;
}

static void
__mia_gldrawable_dispose (GObject *obj)
{
        MiaGLDrawableClass *klass;
        GObjectClass *parent_class;
        MiaGLDrawable *self = (MiaGLDrawable *)obj;

        if (self->dispose_has_run) {
                /* If dispose did already run, return. */
                return;
        }
        /* Make sure dispose does not run twice. */
        self->dispose_has_run = TRUE;
	
	/* add your code here */
       
 /* chain up to the parent class */
       klass = MIA_GLDRAWABLE_CLASS (g_type_class_peek
					    (MIA_TYPE_GLDRAWABLE));
	parent_class =	G_OBJECT_CLASS(g_type_class_peek_parent (klass));
	parent_class->dispose(obj); 
}

static void
__mia_gldrawable_finalize (GObject *obj)
{
        MiaGLDrawableClass *klass;
        GObjectClass *parent_class;
        MiaGLDrawable *self = (MiaGLDrawable *)obj;

        /* add your destruction code here */
	g_string_free(self->name, TRUE);
		
	/* chain up to the parent class */
        klass = MIA_GLDRAWABLE_CLASS (g_type_class_peek
					    (MIA_TYPE_GLDRAWABLE));
	parent_class =	G_OBJECT_CLASS(g_type_class_peek_parent (klass));
	parent_class->finalize(obj); 
}

static void __gl_attach(MiaGLDrawable *self)
{
	self->is_attached = TRUE; 
}

static void __gl_detach(MiaGLDrawable *self)
{
	self->is_attached = FALSE; 
}

static void __initialize(MiaGLDrawable *UNUSED(self))
{
}

static void __gl_draw(MiaGLDrawable *self, MiauiGLView *UNUSED(view), gboolean UNUSED(high_resolution))
{
	if (!self->is_attached) {
		g_warning("try to draw an un-attached object");
		return; 
	}
	if (!self->is_visible)
		return; 
}

void
mia_gldrawable_gl_initialize(MiaGLDrawable *self)
{
	g_assert(MIA_IS_GLDRAWABLE(self));
	
	MIA_GLDRAWABLE_GET_CLASS(self)->initialize(self);
}


void
mia_gldrawable_gl_attach(MiaGLDrawable *self)
{
	g_assert(MIA_IS_GLDRAWABLE(self));
	
	MIA_GLDRAWABLE_GET_CLASS(self)->gl_attach(self);
}

void mia_gldrawable_gl_detach(MiaGLDrawable *self)
{
	g_assert(MIA_IS_GLDRAWABLE(self));
	MIA_GLDRAWABLE_GET_CLASS(self)->gl_detach(self);
}

void mia_gldrawable_gl_draw(MiaGLDrawable *self, MiauiGLView *view, gboolean high_resolution)
{
	g_assert(MIA_IS_GLDRAWABLE(self));
	MIA_GLDRAWABLE_GET_CLASS(self)->gl_draw(self, view, high_resolution);
}

const gchar *
mia_gldrawable_get_name(MiaGLDrawable *self)
{
	g_assert(MIA_IS_GLDRAWABLE(self));
	return self->name->str; 
}

static void
__mia_gldrawable_class_init (gpointer g_class, gpointer UNUSED(g_class_data))
{
	GParamSpec *pspec;
	
	MiaGLDrawableClass *klass = MIA_GLDRAWABLE_CLASS(g_class);
	GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);

        gobject_class->set_property = __mia_gldrawable_set_property;
        gobject_class->get_property = __mia_gldrawable_get_property;
        gobject_class->dispose = __mia_gldrawable_dispose;
        gobject_class->finalize = __mia_gldrawable_finalize;
        gobject_class->constructor = __mia_gldrawable_constructor;

	/* add your code here (e.g. define properties) */
	klass->gl_attach = __gl_attach; 
	klass->gl_detach = __gl_detach; 
	klass->gl_draw   = __gl_draw;
	klass->initialize= __initialize; 
	
	pspec = g_param_spec_string (mia_gldrawable_name_property, 
				     "name","drawable name", 
				     "(unnamed)", G_PARAM_READWRITE);	
	g_object_class_install_property (gobject_class, prop_name, pspec);

}

gboolean mia_gldrawable_test(void)
{
	gboolean result = FALSE;
	MiaGLDrawable *test = mia_gldrawable_new("test");
	g_return_val_if_fail(test, FALSE);
	result = MIA_IS_GLDRAWABLE(test);
	
	/* add your tests here */
	
	
	g_object_unref(G_OBJECT(test));
	return result;
}

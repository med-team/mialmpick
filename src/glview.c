/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */
//#define DRAW_DEBUG

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <GL/gl.h>
#include <GL/glu.h>
//#include <Cg/cg.h>
//#include <Cg/cgGL.h>
#include <gtk/gtkgl.h>
#include <globals.h>

#include <math.h>
#include <string.h>
#include <stdio.h>

#include "glview.h"

#include "trackball.h"
#include "landmarktable.h"
#include "landmarkinput.h"
#include "globals.h"
#include "gldrawable.h"
#include "png_save.h"

typedef enum _Commands Commands;
enum _Commands { es_init,
	es_frustum,
	es_redraw,
	es_destroy,
	es_new3dtexture,
	es_newtransfer,
	es_getcoord,
	es_attach,
	es_unrealize,
	es_connect_object,
	es_snapshot
};

typedef struct {
	GMutex mutex;
	GCond cond;
	gboolean free;
} AccessStruc;

typedef struct _GLViewSignal GLViewSignal;

struct _GLViewSignal {
	Commands command;
	gpointer data;
};

typedef void (*mouse_f) (MiauiGLView * self, gint x, gint y);

int max_texture_size = 1024; 


struct _MiauiGLView {
	GtkDrawingArea parent;
	gboolean dispose_has_run;

	GHashTable *drawables;

	/* OpenGL elements */
	MiaCamera *camera;
	MiaVector3d *lightpos;

	GLdouble gl_modelview_matrix[16];
	GLdouble gl_project_matrix[16];
	RotMatrix gl_rot_matrix;

	/* mouse motion */
	mouse_f lmouse_move_handler;
	gint mouse_old_x;
	gint mouse_old_y;
	gboolean l_mouse_dragging;
	gboolean m_mouse_dragging;
	MiaQuaternion *rot_helper;
	gboolean zoomin_enabled;
	gboolean abort_rendering; 
	GMutex   abort_rendering_mutex;

	/* Landmark picking */
	GtkWidget *landmark_menu;
	SetLandmarkCallback set_landmark;


	/* GL stuff */
	gfloat clipplane_far;
	gfloat clipplane_near;
	GThread *gl_thread;

	/* Thread handling */
	GAsyncQueue *signal_queue;
	AccessStruc view_access;
	AccessStruc data_access;
	/* GL Thread specific */
	GTimer *perf_timer;
	gboolean use_low;
	
	gdouble back_color[4];

	MiaVector3d *rotation_center;
	gboolean mb_down; 
};

static void
  __send_wait (MiauiGLView * self, AccessStruc * ac, Commands command, gpointer data);

static GObject *__miaui_glview_constructor (GType type,
					    guint n_construct_properties,
					    GObjectConstructParam *
					    construct_properties);

static void
  __miaui_glview_class_init (gpointer g_class, gpointer g_class_data);

static void
  __miaui_glview_instance_init (GTypeInstance * instance, gpointer g_class);

GType
miaui_glview_get_type (void)
{
	static GType type = 0;
	if (type == 0) {
		static const GTypeInfo info = {
			sizeof (MiauiGLViewClass),
			NULL,	/* base_init */
			NULL,	/* base_finalize */
			__miaui_glview_class_init,	/* class_init */
			NULL,	/* class_finalize */
			NULL,	/* class_data */
			sizeof (MiauiGLView),
			0,	/* n_preallocs */
			__miaui_glview_instance_init,	/* instance_init */
			NULL
		};
		type = g_type_register_static (GTK_TYPE_DRAWING_AREA,
					       "MiauiGLViewType", &info, 0);
	}
	return type;
}




GtkWidget *
miaui_glview_new (void)
{
	MiauiGLView *obj =
		(MiauiGLView *) g_object_new (MIAUI_TYPE_GLVIEW, NULL);
	/* add your code here */

	gtk_widget_show(GTK_WIDGET (obj)); 
	return GTK_WIDGET (obj);
}

OptimalDirection
miaui_glview_get_draw_direction (MiauiGLView * self)
{
	if (self->gl_modelview_matrix[2] >= 0.5)
		return od_xm;
	else if (self->gl_modelview_matrix[2] <= -0.5)
		return od_xp;
	else if (self->gl_modelview_matrix[6] >= 0.5)
		return od_ym;
	else if (self->gl_modelview_matrix[6] <= -0.5)
		return od_yp;
	else if (self->gl_modelview_matrix[10] >= 0.5)
		return od_zm;
	else if (self->gl_modelview_matrix[10] <= -0.5)
		return od_zp;
	else
		return od_unknown;
}

MiaVector3d *
miaui_glview_get_light_direction (MiauiGLView * self, MiaVector3d * result)
{
	float norm = 0;
	int i;
	float lp[3];
	for (i = 0; i < 3; ++i) {
		lp[i] = self->gl_rot_matrix[i][0] * self->lightpos->x +
			self->gl_rot_matrix[i][1] * self->lightpos->y +
			self->gl_rot_matrix[i][2] * self->lightpos->z;
		norm += lp[i] * lp[i];
	}
	norm =  sqrt (norm);
	lp[0] /= norm;
	lp[1] /= norm;
	lp[2] /= norm;

	if (result) {
		result->x = lp[0];
		result->y = lp[1];
		result->z = lp[2];
	} else
		result = mia_vector3d_new (lp[0], lp[1], lp[2]);

	return result;
}

void
miaui_glview_del_drawable (MiauiGLView * self, gpointer key)
{
	g_assert (MIAUI_IS_GLVIEW (self));

	g_hash_table_remove (self->drawables, key);
}

gdouble *miaui_glview_get_back_color_ptr(MiauiGLView * self)
{
	return self->back_color;
}


static void __send (MiauiGLView * self, Commands command, gpointer data);

void
lmouse_rotate (MiauiGLView * self, gint x, gint y)
{

	float q[4];
	float rot[4];

	GtkWidget *w = GTK_WIDGET (self);

	MiaQuaternion *r = mia_camera_get_rotation (self->camera);
	MiaVector3d *v = MIA_VECTOR3D (r);
	float width = w->allocation.width;
	float height = w->allocation.height;

	trackball (q,
		   (2.0f * self->mouse_old_x - width) / width,
		   (height - 2.0f * self->mouse_old_y) / height,
		   (2.0f * x - width) / width,
		   (height - 2.0f * y) / height, 1.0);

	rot[0] = v->x;
	rot[1] = v->y;
	rot[2] = v->z;
	rot[3] = r->r;

	add_quats (q, rot, rot);

	v->x = rot[0];
	v->y = rot[1];
	v->z = rot[2];
	r->r = rot[3];

	mia_quaternion_get_rotation (r, self->gl_rot_matrix);
}

void
lmouse_shift (MiauiGLView * self, gint x, gint y)
{

	MiaVector3d *l = mia_camera_get_location (self->camera);
	l->x += (x - self->mouse_old_x) * 0.01;
	l->y += (self->mouse_old_y - y) * 0.01;
}

#define UNREF(func, obj) if (obj) { func(obj); obj = NULL; }

void 
miaui_glview_set_rotation_center(MiauiGLView *self, MiaVector3d *center)
{
	g_assert(center); 
	MiaVector3d *help = self->rotation_center; 
	self->rotation_center = center; 
	UNREF (g_object_unref, help); 
}


void
lmouse_zoom (MiauiGLView * self, gint x, gint y)
{
	float zoom = mia_camera_get_zoom (self->camera);
	float dist = mia_camera_get_dist (self->camera);

	zoom += (self->mouse_old_x - x) / 50.0;

	if (zoom < 0)
		zoom = 0.1;

	dist += (y - self->mouse_old_y) / 50.0f;

	mia_camera_set_zoom (self->camera, zoom);

	mia_camera_set_dist (self->camera, dist);

	glview_unlock(); 
	__send_wait (self, &self->view_access, es_frustum, NULL);
	glview_lock(); 
}

void
miaui_glview_reset_camera (MiauiGLView * self)
{
	mia_quaternion_get_rotation (mia_camera_get_rotation (self->camera), self->gl_rot_matrix);
	glview_unlock(); 		
	__send_wait (self, &self->view_access, es_frustum, NULL);
	glview_lock(); 
	gtk_widget_queue_draw (GTK_WIDGET (self));
}	

void
miaui_glview_set_camera (MiauiGLView * self, MiaCamera * camera)
{
	g_assert (MIAUI_IS_GLVIEW (self));
	if (camera) {
		if (self->camera)
			mia_camera_copy (self->camera, camera);
		else
			self->camera = mia_camera_dup (camera);

		miaui_glview_reset_camera(self);
	}
}

void
miaui_glview_set_mouse_action (MiauiGLView * self,
			       MiauiGLViewMouseAction action)
{
	g_assert (MIAUI_IS_GLVIEW (self));
	switch (action) {
	case miaui_glview_rotate:
		self->lmouse_move_handler = lmouse_rotate;
		break;
	case miaui_glview_shift:
		self->lmouse_move_handler = lmouse_shift;
		break;
	case miaui_glview_zoom:
		self->lmouse_move_handler = lmouse_zoom;
		break;
	default:
		g_assert_not_reached ();
	}
}

void 
miaui_glview_enable_zoomin(MiauiGLView *self)
{
	g_assert(MIAUI_IS_GLVIEW(self));
	self->zoomin_enabled = TRUE; 
}
	
void
miaui_glview_disable_zoomin(MiauiGLView *self)
{
	g_assert(MIAUI_IS_GLVIEW(self));
	self->zoomin_enabled = FALSE; 
	
}


GtkWidget *
miaui_glview_custum_create (gchar * UNUSED(widget_name), gchar * UNUSED(string1),
			    gchar * UNUSED(string2), gint UNUSED(int1), gint UNUSED(int2))
{
	return miaui_glview_new ();
}

typedef struct _SnapData SnapData;
struct 	_SnapData {
	gchar *filename;
	gboolean result; 
};

gboolean
miaui_glview_snapshot (MiauiGLView * self, gchar *filename)
{
	SnapData data = {filename, FALSE};
	glview_unlock(); 
	__send_wait (self, &self->view_access, es_snapshot, &data);
	glview_lock(); 
	return data.result;
		
}

MiaCamera *
miaui_glview_get_camera (MiauiGLView * self)
{
	g_assert (MIAUI_IS_GLVIEW (self));
	return self->camera;
}

static void
__send (MiauiGLView * self, Commands command, gpointer data)
{

	GLViewSignal *signal = g_malloc0 (sizeof (GLViewSignal));
	signal->command = command;
	signal->data = data;
	g_async_queue_push (self->signal_queue, signal);
}

static void
__access_free (AccessStruc * ac)
{
	g_mutex_lock (&ac->mutex);
	ac->free = TRUE;
	g_cond_signal (&ac->cond);
	g_debug("thread %p: free lock %p", g_thread_self(), ac);  
	g_mutex_unlock (&ac->mutex);
}

static void
__access_unfree (AccessStruc * ac)
{
	g_mutex_lock (&ac->mutex);
	g_debug("thread %p: unfree lock %p", g_thread_self(), ac);  
	ac->free = FALSE;
	g_mutex_unlock (&ac->mutex);
}

static void
__access_waitfree (AccessStruc * ac)
{
	g_mutex_lock (&ac->mutex);
	g_debug("thread %p: wait for %p", g_thread_self(), ac);  
	while (ac->free == FALSE)
		g_cond_wait (&ac->cond, &ac->mutex);
	g_mutex_unlock (&ac->mutex);
}

static void

__send_wait (MiauiGLView * self, AccessStruc * ac,
	     Commands command, gpointer data)
{
	__access_unfree (ac);
	
	__send (self, command, data);
	__access_waitfree (ac);
	
}

static void
__abort_rendering(MiauiGLView * self)
{
	g_mutex_lock(&self->abort_rendering_mutex);
	self->abort_rendering = TRUE;
	g_mutex_unlock(&self->abort_rendering_mutex);
}

gboolean 
miaui_glview_check_abort_rendering(MiauiGLView *self)
{
	gboolean retval; 
	if (!self->use_low) 
		return FALSE; 
	g_mutex_lock(&self->abort_rendering_mutex);
	retval = self->abort_rendering;
	self->abort_rendering = FALSE;
	g_mutex_unlock(&self->abort_rendering_mutex);
	return retval; 
}

static void

__miaui_glview_set_property (GObject * object,
			     guint property_id,
			     const GValue * UNUSED(value), GParamSpec * pspec)
{
	/*
	 * MiauiGLView *self = (MiauiGLView *) object;
	 * GObjectClass *klass; 
	 */
	switch (property_id) {
		/* handle your properties here */
	default:
		/* We don't have any other property... */
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object,
						   property_id, pspec);
	}
}



static void

__miaui_glview_get_property (GObject * object,
			     guint property_id,
			     GValue * UNUSED(value), GParamSpec * pspec)
{
	/*
	 * MiauiGLView *self = (MiauiGLView *) object;
	 * GObjectClass *klass; 
	 */
	switch (property_id) {
		/* handle your properties here */
	default:
		/* We don't have any other property... */
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object,
						   property_id, pspec);
	}
}


static gpointer __glview_job (gpointer * w);

gboolean
__on_button_press_event (GtkWidget * widget,
			 GdkEventButton * event, gpointer UNUSED(user_data))
{
	MiauiGLView *self = MIAUI_GLVIEW (widget);
	self->mouse_old_x = event->x;
	self->mouse_old_y = event->y;
	if (event->button == 3) {
		/* show the Landmark menu  */
		gtk_menu_popup (GTK_MENU (self->landmark_menu), NULL,
				NULL, NULL, self, GDK_BUTTON3_MASK,
				gtk_get_current_event_time ());
	}
	if (event->button == 1 || event->button == 2) 
		self->mb_down = TRUE; 

	return TRUE;
}

void 
 __do_zoomin(MiauiGLView *self)
{
	/* use the current mouse location to shift the object 
	   into the middle and increase zoom                 __send_wait (self, &self->view_access, es_frustum, NULL); */ 
	float zoom ;
	gfloat dx = self->mouse_old_x - GTK_WIDGET(self)->allocation.width/2;
	gfloat dy = GTK_WIDGET(self)->allocation.height/2 - self->mouse_old_y;
	MiaVector3d *l = mia_camera_get_location (self->camera);
	
	l->x -= dx/ 1000;
	l->y -= dy/ 1000;
	
	zoom = mia_camera_get_zoom (self->camera);

	zoom *= 0.5;
	if (zoom < 0.1)
		zoom = 0.1;

	mia_camera_set_zoom (self->camera, zoom);
	glview_unlock(); 
	__send_wait (self, &self->view_access, es_frustum, NULL);
	glview_lock(); 
	gtk_widget_queue_draw (GTK_WIDGET(self));

}

gboolean
__on_button_release_event (GtkWidget * widget,
			   GdkEventButton * event, gpointer UNUSED(user_data))
{
	MiauiGLView *self = MIAUI_GLVIEW (widget);
	if (event->button == 1) {
		self->l_mouse_dragging = FALSE;
		if (self->zoomin_enabled)	
			__do_zoomin(self);
		
	}
	if (event->button == 2 && self->m_mouse_dragging) {
		self->l_mouse_dragging = FALSE;
	}
	if (event->button == 1 || event->button == 2)
		self->mb_down = FALSE; 

	return TRUE;
}

gboolean
__on_motion_notify_event (MiauiGLView * self,
			  GdkEventMotion * event, gpointer UNUSED(user_data))
{
	if (event->state & GDK_BUTTON1_MASK || self->l_mouse_dragging) {
		if (event->is_hint) {
			int x, y;
			GdkModifierType state;


			gdk_window_get_pointer (event->window, &x, &y,
						&state);

			if (self->lmouse_move_handler)
				self->lmouse_move_handler (self, x, y);

			self->mouse_old_x = x;
			self->mouse_old_y = y;
		} else {
			/* set widget! */
			self->zoomin_enabled = FALSE; 
			
			self->l_mouse_dragging = TRUE;
			self->mouse_old_x = event->x;
			self->mouse_old_y = event->y;
		}

		gtk_widget_queue_draw (GTK_WIDGET (self));
	}
	if (event->state & GDK_BUTTON2_MASK || self->m_mouse_dragging) {
		if (event->is_hint) {
			int x, y;
			GdkModifierType state;


			gdk_window_get_pointer (event->window, &x, &y,
						&state);

			lmouse_shift (self, x, y);

			self->mouse_old_x = x;
			self->mouse_old_y = y;
		} else {
			self->m_mouse_dragging = TRUE;
			self->mouse_old_x = event->x;
			self->mouse_old_y = event->y;
		}

		gtk_widget_queue_draw (GTK_WIDGET (self));
		return TRUE;
	}

	return FALSE;
}

gboolean
__on_configure_event (GtkWidget * w, gpointer UNUSED(user_data))
{
	MiauiGLView *self = MIAUI_GLVIEW (w);
	glview_unlock(); 
	__send_wait (self, &self->view_access, es_frustum, NULL);
	glview_lock(); 
	return FALSE;
}

void
__on_realize (GtkWidget * w, gpointer UNUSED(user_data))
{
	g_debug("__on_realize start"); 

	__send_wait (MIAUI_GLVIEW (w), &MIAUI_GLVIEW (w)->view_access,
		     es_init, NULL);
	g_signal_connect (w, "configure_event",
			  G_CALLBACK (__on_configure_event), NULL);
	g_message("Max texture size: %d", max_texture_size); 
	g_debug("__on_realize end"); 

}

void
__on_unrealize (GtkWidget * w, gpointer UNUSED(user_data))
{
	MiauiGLView *self = MIAUI_GLVIEW (w);
	if (!self->dispose_has_run) {
		glview_unlock();
		__send_wait (self, &self->view_access, es_unrealize, NULL);
		glview_lock();
	}
}


void
__on_expose (GtkWidget * w, gpointer UNUSED(user_data))
{
	g_debug("__on_expose"); 
	__abort_rendering(MIAUI_GLVIEW (w));
	glview_unlock(); 
	__send_wait (MIAUI_GLVIEW (w), &MIAUI_GLVIEW (w)->view_access,
		     es_redraw, NULL);
	glview_lock(); 
}

static void
__do_add_lm (const gchar *lmname, MiaLandmark *lm, MiauiLandmarkTable *lm_table, GtkWidget *view)
{
	mia_landmark_set_name(lm, lmname);
	miaui_landmark_table_add_landmark (lm_table, lm);
	gtk_widget_queue_draw (view);
}

static void
__add_landmark (GtkMenuItem * UNUSED(item), gpointer user_data)
{
	MiaVector3d *location = NULL;
	MiauiLandmarkInput *lmi;

	MiauiGLView *self = MIAUI_GLVIEW (user_data);
	glview_unlock(); 
	__send_wait (self, &self->data_access, es_getcoord, &location);
	glview_lock(); 

	if (location) {
		MiaCamera *c = mia_camera_dup (self->camera);
		MiaLandmark *lm =
			mia_landmark_new ("", location, iso_value_min, c,
					  NULL);

		lmi = miaui_landmark_input_new (_("Add new landmark"), lm);
		miaui_landmark_input_set_add_callback (lmi,
						       __do_add_lm,
						       landmarktable, GTK_WIDGET(self));
		gtk_widget_show (GTK_WIDGET (lmi));
	}

}

static void
__set_landmark (GtkMenuItem * UNUSED(item), gpointer user_data)
{

	MiaVector3d *location = NULL;
	MiauiGLView *self = MIAUI_GLVIEW (user_data);
	g_assert (self);
	glview_unlock(); 
	__send_wait (self, &self->data_access, es_getcoord, &location);
	glview_lock(); 
	if (location) {
		MiauiGLViewClass *klass = MIAUI_GLVIEW_GET_CLASS (self);
		g_signal_emit (self, klass->landmark_picked_id, 0,
			       location, NULL);
		/* unref, because the signal might go away unhandled */
		g_object_unref(location);
	}
}

static void
__create_landmark_menu (MiauiGLView * self)
{
	GtkWidget *child;
	self->landmark_menu = gtk_menu_new ();

	child = gtk_menu_item_new_with_label (_("Set Landmark"));
	g_signal_connect (child, "activate",
			  G_CALLBACK (__set_landmark), self);
	gtk_widget_show (child);
	gtk_menu_append (self->landmark_menu, child);

	child = gtk_menu_item_new_with_label (_("Add Landmark"));
	g_signal_connect (child, "activate",
			  G_CALLBACK (__add_landmark), self);
	gtk_widget_show (child);
	gtk_menu_append (self->landmark_menu, child);

}

static gboolean
__scroll_event (GtkWidget * widget,
		GdkEventScroll * event, gpointer UNUSED(user_data))
{
	MiauiGLView *self = MIAUI_GLVIEW (widget);
	float zoom = mia_camera_get_zoom (self->camera);

	if (event->direction == GDK_SCROLL_UP) {
		zoom += 0.05;
	} else if (event->direction == GDK_SCROLL_DOWN) {
		zoom -= 0.05;
		if (zoom < 0.1)
			zoom = 0.1;

	}
	mia_camera_set_zoom (self->camera, zoom);
	glview_unlock(); 
	__send_wait (self, &self->view_access, es_frustum, NULL);
	glview_lock(); 
	gtk_widget_queue_draw (widget);

	return TRUE;
}



static void
__miaui_glview_instance_init (GTypeInstance * instance, gpointer UNUSED(g_class))
{

	MiauiGLView *self = (MiauiGLView *) instance;
	GtkWidget *w = GTK_WIDGET (self);
	self->dispose_has_run = FALSE;
	self->mb_down = FALSE; 

	g_debug("__miaui_glview_instance_init"); 

	/* Thread handling */
	self->signal_queue = g_async_queue_new ();
	g_async_queue_ref (self->signal_queue);

	g_mutex_init(&self->view_access.mutex);
	g_cond_init(&self->view_access.cond);
	self->view_access.free = TRUE;

	g_mutex_init(&self->data_access.mutex);
	g_cond_init(&self->data_access.cond);
	self->data_access.free = TRUE;

	self->rotation_center = mia_vector3d_new(1,1,1); 

	__access_unfree(&self->view_access); 

	/* create the GL thread, this also sets the GL capability */
	self->gl_thread =
		g_thread_new ("renderthread", (GThreadFunc) __glview_job, self);

	/* wait for the worker thread to be created   */
	__access_waitfree(&self->view_access); 


	self->lmouse_move_handler = lmouse_rotate;

	self->lightpos = mia_vector3d_new (-10, 2, -24);
	self->camera = mia_camera_new (mia_vector3d_new (0, 0, 250),
				       mia_quaternion_new (0.0, 0.0,
							   0.0, 1.0), 1.0);
	self->clipplane_far = 300;
	self->clipplane_near = 200;
	/* mia_quaternion_new(-0.212891, 0.731839, -0.615647, 0.200170), */
	self->rot_helper = mia_quaternion_new (0.0, 0.0, 0.0, 1.0);

	gtk_widget_set_events (GTK_WIDGET (self),
			       GDK_EXPOSURE_MASK |
			       GDK_POINTER_MOTION_MASK |
			       GDK_POINTER_MOTION_HINT_MASK |
			       GDK_BUTTON_MOTION_MASK |
			       GDK_BUTTON1_MOTION_MASK |
			       GDK_BUTTON2_MOTION_MASK |
			       GDK_BUTTON3_MOTION_MASK |
			       GDK_BUTTON_PRESS_MASK |
			       GDK_BUTTON_RELEASE_MASK);

	/* link the signals */
	self->l_mouse_dragging = FALSE;

	__create_landmark_menu (self);

	g_signal_connect (w, "button-release-event",
			  G_CALLBACK (__on_button_release_event), NULL);

	g_signal_connect (w, "button-press-event",
			  G_CALLBACK (__on_button_press_event), NULL);

	g_signal_connect (w, "motion-notify-event",
			  G_CALLBACK (__on_motion_notify_event), NULL);

	g_signal_connect (w, "realize", G_CALLBACK (__on_realize), NULL);
	g_signal_connect (w, "unrealize", G_CALLBACK (__on_unrealize), NULL);
	g_signal_connect (w, "expose-event", G_CALLBACK (__on_expose), NULL);
	g_signal_connect (w, "scroll-event",
			  G_CALLBACK (__scroll_event), NULL);

	/* initialize rotation matrix */
	mia_quaternion_get_rotation (mia_camera_get_rotation
				     (self->camera), self->gl_rot_matrix);


	self->drawables =
		g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
				       g_object_unref);


	self->m_mouse_dragging = FALSE;
	self->l_mouse_dragging = FALSE;

	self->abort_rendering = FALSE; 
	g_mutex_init(&self->abort_rendering_mutex);

	self->use_low = TRUE;


	memset(self->back_color, 0, 4 * sizeof(gdouble));


	
}

static GObject *
__miaui_glview_constructor (GType type,
			    guint
			    n_construct_properties,
			    GObjectConstructParam * construct_properties)
{
	GObject *obj;
	{
		/* Invoke parent constructor. */
		MiauiGLViewClass *klass;
		GObjectClass *parent_class;
		klass = MIAUI_GLVIEW_CLASS (g_type_class_peek
					    (MIAUI_TYPE_GLVIEW));
		parent_class =
			G_OBJECT_CLASS (g_type_class_peek_parent (klass));
		obj = parent_class->constructor (type,
						 n_construct_properties,
						 construct_properties);
	}

	/* add your code here */

	return obj;
}

static void
__miaui_glview_dispose (GObject * obj)
{
	MiauiGLViewClass *klass;
	GObjectClass *parent_class;
	MiauiGLView *self = (MiauiGLView *) obj;

	if (self->dispose_has_run) {
		/* If dispose did already run, return. */
		return;
	}
	/* Make sure dispose does not run twice. */
	self->dispose_has_run = TRUE;

	/* end the rendering thread */
	__send (self, es_destroy, NULL);
	g_thread_join (self->gl_thread);
	
	/* clean up connected drawables */
	g_hash_table_destroy (self->drawables);
	self->drawables = NULL;

	/* add your code here */
	UNREF (g_object_unref, self->camera);
	UNREF (g_object_unref, self->lightpos);
	UNREF (g_object_unref, self->rot_helper);
	UNREF (g_async_queue_unref, self->signal_queue);
	
	UNREF (g_object_unref, self->rotation_center); 

	/* chain up to the parent class */
	klass = MIAUI_GLVIEW_CLASS (g_type_class_peek (MIAUI_TYPE_GLVIEW));
	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	parent_class->dispose (obj);
	
}

#undef UNREF

static void
__miaui_glview_finalize (GObject * obj)
{
	MiauiGLViewClass *klass;
	GObjectClass *parent_class;

	/* add your destruction code here */

	/* chain up to the parent class */
	klass = MIAUI_GLVIEW_CLASS (g_type_class_peek (MIAUI_TYPE_GLVIEW));
	parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (klass));
	parent_class->finalize (obj);
}

static void
__miaui_glview_class_init (gpointer g_class, gpointer UNUSED(g_class_data))
{
	GType param_types[1];
	GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
	MiauiGLViewClass *klass = MIAUI_GLVIEW_CLASS (g_class);

	gobject_class->set_property = __miaui_glview_set_property;
	gobject_class->get_property = __miaui_glview_get_property;
	gobject_class->dispose = __miaui_glview_dispose;
	gobject_class->finalize = __miaui_glview_finalize;
	gobject_class->constructor = __miaui_glview_constructor;

	param_types[0] = G_TYPE_POINTER;

	klass->landmark_picked_id =
		g_signal_newv ("location-picked",
			       G_TYPE_FROM_CLASS (g_class),
			       G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE
			       | G_SIGNAL_NO_HOOKS, NULL, NULL, NULL,
			       g_cclosure_marshal_VOID__POINTER,
			       G_TYPE_NONE, 1, param_types);
	/* add your code here (e.g. define properties) */


}

void
miaui_glview_update_drawable (MiauiGLView * self, gpointer drawable)
{
	g_assert (MIAUI_IS_GLVIEW (self));
	g_assert (MIA_IS_GLDRAWABLE (drawable));

	if (gtk_widget_get_realized(GTK_WIDGET(self)))
		glview_unlock();
	__send_wait (self, &self->view_access, es_connect_object, drawable);
	if (gtk_widget_get_realized(GTK_WIDGET(self)))
		glview_lock(); 
}

void
miaui_glview_add_drawable (MiauiGLView * self, gpointer drawable)
{
	g_assert (MIAUI_IS_GLVIEW (self));
	g_assert (MIA_IS_GLDRAWABLE (drawable));

	g_hash_table_insert (self->drawables, (gpointer)
			     mia_gldrawable_get_name (drawable), drawable);
	g_object_ref (drawable);

	if (gtk_widget_get_realized(GTK_WIDGET(self)))
	    glview_unlock(); 
	
	__send (self, es_attach, drawable);
	
	if (gtk_widget_get_realized(GTK_WIDGET(self)))
		glview_lock(); 
}

gboolean
miaui_glview_test (void)
{
	gboolean result = FALSE;
	MiauiGLView *test = MIAUI_GLVIEW (miaui_glview_new ());
	g_return_val_if_fail (test, FALSE);
	result = MIAUI_IS_GLVIEW (test);

	/* add your tests here */


	g_object_unref (G_OBJECT (test));
	return result;
}

/* OpenGL  thread specifics */

void
glview_lock ()
{
	g_debug("lock GDK"); 
	gdk_threads_enter ();
	g_debug("  got it"); 
}

void
glview_unlock ()
{
	g_debug("unlock GDK"); 
	gdk_threads_leave ();
}

GdkGLDrawable *
__gl_begin (MiauiGLView * self)
{
	GdkGLContext *glcontext;
	GdkGLDrawable *gldrawable;

	g_debug("__gl_begin begin"); 
	glview_lock ();
	

	glcontext = gtk_widget_get_gl_context (GTK_WIDGET (self));
	if (!glcontext) {
		g_warning ("glview_render: not a glcontext %p", self);
		return NULL;
	}

	gldrawable = gtk_widget_get_gl_drawable (GTK_WIDGET (self));
	if (!gldrawable) {
		g_warning ("glview_render: no gldrawable");
		return NULL;
	}

	if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)) {
		g_warning ("glview_render: something is really wrong");
		return NULL;
	}

	glview_unlock ();
	g_debug("__gl_begin end"); 

	return gldrawable;
}

void
__gl_swap (GdkGLDrawable * gldrawable)
{
	glview_lock ();
	gdk_gl_drawable_swap_buffers (gldrawable);
	glview_unlock ();
}

void
__gl_end (GdkGLDrawable * gldrawable)
{
	glview_lock ();
	gdk_gl_drawable_gl_end (gldrawable);
	glview_unlock ();
}



static void
__init_gl_context (GtkWidget * w)
{
	GdkGLConfig *glconfig;

	g_return_if_fail (w != NULL);
	
	glview_lock ();
	g_debug("__init_gl_context begin"); 



	/** firsttimer */
	glconfig = gdk_gl_config_new_by_mode (GDK_GL_MODE_RGB |
					      GDK_GL_MODE_DOUBLE |
					      GDK_GL_MODE_DEPTH);
	if (!glconfig) {
		g_error ("Cannot find an appropriated visual");
		exit (1);
	}
	if (!gtk_widget_set_gl_capability (w,
					   glconfig,
					   NULL, TRUE, GDK_GL_RGBA_TYPE))
		g_warning ("unable to establish gl cabability");
	glview_unlock ();
	g_debug("__init_gl_context end"); 
	__access_free(&MIAUI_GLVIEW(w)->view_access); 
}

static void
__release_gl_context(GtkWidget * w)
{
	gdk_window_unset_gl_capability (w->window);
}

static void

__gl_attach_drawable (gpointer UNUSED(key), gpointer value, gpointer UNUSED(user_data))
{
	MiaGLDrawable *drawable = MIA_GLDRAWABLE (value);
	if (!drawable)
		return;

	mia_gldrawable_gl_attach (drawable);
}

void
__glview_attach (MiauiGLView * self)
{
	/*GLenum error;*/
	glClearColor (0.0, 0.0, 0.0, 0.0);
	/* add all allocated rendering drawables to the OpenGL context */
	g_hash_table_foreach (self->drawables, __gl_attach_drawable, self);

}

static void

__gl_detach_drawable (gpointer UNUSED(key), gpointer value, gpointer UNUSED(user_data))
{
	MiaGLDrawable *drawable = MIA_GLDRAWABLE (value);
	if (!drawable)
		return;

	mia_gldrawable_gl_detach (drawable);
}

void
__glview_detach (MiauiGLView * self)
{
	/* detach all drawables from the GL Context */
	g_hash_table_foreach (self->drawables, __gl_detach_drawable, self);
}

static void
__set_frustum (MiauiGLView * self)
{

	float w, h, zoom;
	GtkWidget *widget = GTK_WIDGET (self);

	zoom = mia_camera_get_zoom (self->camera);

	glViewport (0, 0, widget->allocation.width,
		    widget->allocation.height);

	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();


	if (widget->allocation.width > widget->allocation.height) {
		w = (zoom * widget->allocation.width) /
			widget->allocation.height;
		h = zoom;
	} else {
		w = zoom;
		h = (zoom * widget->allocation.height) /
			widget->allocation.width;
	}
	glFrustum (-w, w, -h, h, self->clipplane_near, self->clipplane_far);


	glGetDoublev (GL_PROJECTION_MATRIX, self->gl_project_matrix);
}

#ifdef DRAW_DEBUG
static void
__draw_frame ()
{
	glBegin (GL_LINE_STRIP);

	glColor3f (1.0, 0, 0);

	glVertex3f (-1.0, -1.0, -1.0);
	glVertex3f (-1.0, -1.0,  1.0);
	glVertex3f (-1.0,  1.0,  1.0);
	glVertex3f (-1.0,  1.0, -1.0);
	glVertex3f (-1.0, -1.0, -1.0);
	glVertex3f ( 1.0, -1.0, -1.0);

	glVertex3f ( 1.0, -1.0,  1.0);
	glVertex3f ( 1.0,  1.0,  1.0);
	glVertex3f ( 1.0,  1.0, -1.0);
	glVertex3f ( 1.0, -1.0, -1.0);

	glEnd ();

	glBegin (GL_LINES);

	glVertex3f (-1.0,  1.0,  1.0);
	glVertex3f ( 1.0,  1.0,  1.0);

	glVertex3f (-1.0,  1.0, -1.0);
	glVertex3f ( 1.0,  1.0, -1.0);

	glVertex3f (-1.0, -1.0,  1.0);
	glVertex3f ( 1.0, -1.0,  1.0);

	glEnd ();

}
#endif /*DRAW_DEBUG*/

typedef struct _RenderDrawables RenderDrawables;
struct _RenderDrawables {
	MiauiGLView *view;
	gboolean high_res;
};


void

__render_drawables_opaque (gpointer UNUSED(key), gpointer value, gpointer user_data)
{
	RenderDrawables *rd;
	MiaGLDrawable *d = MIA_GLDRAWABLE (value);
	if (!d)
		return;
	rd = (RenderDrawables *) user_data;

	if (!mia_gldrawable_is_transparent(d))
		mia_gldrawable_gl_draw (d, rd->view, rd->high_res);
}

void
__render_drawables_transparent (gpointer UNUSED(key), gpointer value, gpointer user_data)
{
	RenderDrawables *rd;
	MiaGLDrawable *d = MIA_GLDRAWABLE (value);
	if (!d)
		return;
	rd = (RenderDrawables *) user_data;

	if (mia_gldrawable_is_transparent(d)) {
		mia_gldrawable_gl_draw (d, rd->view, rd->high_res);
	}
}

static void
__render_thread (MiauiGLView * self, gboolean use_high_resolution)
{
	GLenum error;
	/* set the camera */
	const GLfloat light_color[4] = { 1.0, 1.0, 1.0, 1.0 };
	RenderDrawables rd = { self, use_high_resolution };
	MiaVector3d *camera_loc = mia_camera_get_location (self->camera);

	miaui_glview_check_abort_rendering(self);	
	
	glDepthMask(GL_TRUE);
	glClearColor(self->back_color[0],
		     self->back_color[1],
	             self->back_color[2],
	             self->back_color[3]);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable (GL_DEPTH_TEST);
	glEnable (GL_LIGHT0);


	glLightfv (GL_LIGHT0, GL_DIFFUSE, light_color);
	error = glGetError ();
	if (error)
		g_warning ("__render_thread b error %s", gluErrorString (error));

	glMatrixMode (GL_MODELVIEW);
	error = glGetError ();
	if (error)
		g_warning ("__render_thread c error %s", gluErrorString (error));

	glLoadIdentity ();
	glTranslatef (camera_loc->x, camera_loc->y, -mia_camera_get_dist (self->camera));
	glMultMatrixf ((GLfloat *) self->gl_rot_matrix);
	glTranslatef(-self->rotation_center->x, -self->rotation_center->y, -self->rotation_center->z);  

	glGetDoublev (GL_MODELVIEW_MATRIX, self->gl_modelview_matrix);

	/* render all drawables */
#ifdef DRAW_DEBUG
	__draw_frame (); 
#endif


	glEnable (GL_LIGHTING);
	error = glGetError ();
	if (error)
		g_warning ("__render_thread 1 error %s", gluErrorString (error));
	


	g_hash_table_foreach (self->drawables, __render_drawables_opaque, &rd);
	
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	
	g_hash_table_foreach (self->drawables, __render_drawables_transparent, &rd);
	glDisable(GL_BLEND);
	
	if (error)
		g_warning ("__render_thread 2 error %s", gluErrorString (error));
	glDisable (GL_LIGHTING);
}

static void
__glview_get_spacecoord (MiauiGLView * self, MiaVector3d ** target)
{
	GLint vp[4];
	GLfloat zvalue;
	GLdouble x, y, z;
	gint iy;

	g_assert (target);

	iy = GTK_WIDGET (self)->allocation.height - self->mouse_old_y;

	glReadBuffer (GL_FRONT);
	glReadPixels (self->mouse_old_x, iy, 1, 1,
		      GL_DEPTH_COMPONENT, GL_FLOAT, &zvalue);

	if (zvalue < 1) {	/* hit something */
		glGetIntegerv (GL_VIEWPORT, vp);
		gluUnProject (self->mouse_old_x, iy, zvalue,
			      self->gl_modelview_matrix,
			      self->gl_project_matrix, vp, &x, &y, &z);
		x = 128.0 * x;
		y = 128.0 * y;
		z = 128.0 * z;

		*target = mia_vector3d_new (x, y, z);

	} else {
		g_debug ("picked emptyness at <%d, %d>", 
			   self->mouse_old_x, self->mouse_old_y);
	}

}

static gboolean 
__do_snapshot (MiauiGLView * self, gchar *filename)
{
	guchar *data;
	GLint vp[4];
	gboolean result;
	
	__render_thread (self, TRUE);
	glFlush ();
	
	glGetIntegerv (GL_VIEWPORT, vp);
	data = g_malloc(vp[2] * vp[3] * 3);
	
	glReadBuffer (GL_BACK);
	glPixelStorei (GL_PACK_ALIGNMENT, 1);
	glReadPixels (0, 0, vp[2], vp[3], GL_RGB, GL_UNSIGNED_BYTE, data);
	
	result = png_save_rgb(filename, vp[2], vp[3], data, FALSE);
	
	g_free(data);
	return result; 
}

static gpointer
__glview_job (gpointer * w)
{
	GdkGLDrawable *gldrawable;
	gboolean running = TRUE;
	gboolean draw_again = FALSE;
	gboolean initialized = FALSE;
	GTimer *perf_timer;

	MiauiGLView *self = MIAUI_GLVIEW (w);

	g_async_queue_ref (self->signal_queue);
	
	__init_gl_context (GTK_WIDGET (self));

	// thread was created, we may continue
	__access_free(&self->view_access); 

	perf_timer = g_timer_new ();

	while (running) {
		GLViewSignal *signal, *last_signal;

		if (self->use_low) {
			signal = (GLViewSignal *)
				g_async_queue_timeout_pop (self->signal_queue, 100000);
			if (!signal) {
				if (initialized && draw_again && self->use_low && !self->mb_down) {
					gldrawable = __gl_begin (self);
					__render_thread (self, TRUE);
					__gl_swap (gldrawable);
					__gl_end (gldrawable);
					draw_again = FALSE;
				}
				continue;
			}
		} else
			signal = (GLViewSignal *)
				g_async_queue_pop (self->signal_queue);
		
		draw_again = TRUE;
		last_signal =
			(GLViewSignal *) g_async_queue_try_pop (self->
								signal_queue);
		
		
		while (last_signal && last_signal->command == signal->command) {
			g_free (signal);
			signal = last_signal;
			last_signal = (GLViewSignal *)
				g_async_queue_try_pop (self->signal_queue);
		}
		
		
		
		switch (signal->command) {
		case es_init:
			g_debug("GL-thread: init"); 
			gldrawable = __gl_begin (self);
			glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, &max_texture_size);
			__glview_attach (self);
			__set_frustum (self);
			__gl_end (gldrawable);
			initialized = TRUE;
			__access_free (&self->view_access);
			break;
		case es_frustum:
			g_debug("GL-thread: frustum"); 
			gldrawable = __gl_begin (self);
			__set_frustum (self);
			__gl_end (gldrawable);
			__access_free (&self->view_access);
			break;
		case es_redraw:
			g_debug("GL-thread: redraw");
			gldrawable = __gl_begin (self);
			__access_free (&self->view_access);
			if (self->use_low)
				draw_again = TRUE;
			__render_thread (self, !self->use_low);
			__gl_swap (gldrawable);
			__gl_end (gldrawable);
			break;
		case es_unrealize:
			g_debug("GL-thread: unrealize"); 
			if (initialized) {
				gldrawable = __gl_begin (self);
				__glview_detach (self);
				__gl_end (gldrawable);
				__release_gl_context(GTK_WIDGET(w));
				__access_free (&self->view_access);
				initialized = FALSE;
			}
			break;
		case es_destroy:
			g_debug("GL-thread: destroy"); 
			if (initialized) {
				gldrawable = __gl_begin (self);
				__glview_detach (self);
				__gl_end (gldrawable);
				__release_gl_context(GTK_WIDGET(w));
				__access_free (&self->view_access);
				initialized = FALSE;
			}
			running = 0;
			break;
		case es_getcoord:
			g_debug("GL-thread: getcoord"); 
			gldrawable = __gl_begin (self);
			__glview_get_spacecoord (self, signal->data);
			__gl_end (gldrawable);
			__access_free (&self->data_access);
			break;
		case es_attach:
			g_debug("GL-thread: attach"); 
			if (initialized) {
				gldrawable = __gl_begin (self);
				mia_gldrawable_gl_attach
					(MIA_GLDRAWABLE (signal->data));
				__gl_end (gldrawable);
			}
			break;
		case es_connect_object:
			if (initialized) {
				gldrawable = __gl_begin (self);
				mia_gldrawable_gl_initialize
					(MIA_GLDRAWABLE (signal->data));
				__gl_end (gldrawable);
			}
			__access_free (&self->view_access);
			break;
		case es_snapshot:
			g_debug("GL-thread: shapshot"); 
			if (initialized) {
				SnapData *data = (SnapData *)signal->data;
				gldrawable = __gl_begin (self);
				data->result = __do_snapshot (self, data->filename);
				__gl_swap (gldrawable);
				__gl_end (gldrawable);
			}else
				g_warning("gl context not initialised");
			__access_free (&self->view_access);
			break;
		default:
			g_warning ("Signal %d not handled", signal->command);
		}
		g_free (signal);
	}

	g_timer_destroy (perf_timer);
	g_async_queue_unref (self->signal_queue);
	
	g_thread_exit (NULL);
	return NULL;
}

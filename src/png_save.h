/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __png_save_h
#define __png_save_h

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <png.h>

#ifdef __cplusplus
extern "C" {
#endif	

	
int png_save(char *name,int w, int h, int bit_depth, int color_type,
	unsigned char *buffer,int line_stride,int top_down);

int png_save_grayscale(char *name,int w, int h,unsigned char *buffer,int top_down);
	
int png_save_rgb(char *name,int w, int h,unsigned char *buffer,int top_down);

#ifdef __cplusplus
}
#endif

#endif

/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <globals.h>
#include "image3d.h"

#include <vistaio.h>

static VistaIOImage get_first_image(const char *file_name) 
{
	VistaIOImage res = NULL; 
	VistaIOAttrList list; 
	VistaIOAttrListPosn posn; 
	FILE *file;
	
	if ((file = fopen(file_name, "r")) == 0) {
		perror(file_name);
		return NULL; 
	}

	list = VistaIOReadFile(file,0);
	fclose(file);
	
	if (!list) 
		VistaIOError("%s is not vista file", file_name); 
	

	for (VistaIOFirstAttr(list, &posn); VistaIOAttrExists(&posn); VistaIONextAttr(&posn)) {
		// it this object an image?
		if (VistaIOGetAttrRepn(&posn) == VistaIOImageRepn) {
			VistaIOExtractAttr(list,VistaIOGetAttrName(&posn), 0, VistaIOImageRepn, &res, 0);
			break;
		}
	}
	VistaIODestroyAttrList(list);
	return res; 
}


MiaImage3d *load_vista_image(const char *filename)
{
	VistaIOStringConst voxel_size = NULL; 
	VistaIOImage img = NULL; 
	MiaImage3d *result = NULL; 
	float sx,sy,sz; 
	
	img = get_first_image(filename);	
	if (!img)
		return NULL; 
	
	switch ( VistaIOPixelRepn(img) ) {
	case  VistaIOShortRepn: 
		result = mia_image3d_new_short(img->ncolumns, img->nrows, img->nbands, VistaIOPixelPtr(img,0,0,0));
		img->data = NULL; 
		break; 
	case VistaIOUByteRepn:
		result = mia_image3d_new_ubyte(img->ncolumns, img->nrows, img->nbands,  VistaIOPixelPtr(img,0,0,0));
		img->data = NULL; 
		break; 
	default:
		g_warning("Input type not supported");
		return NULL; 
	}
	
	if ( VistaIOGetAttr( VistaIOImageAttrList(img), "voxel", NULL, VistaIOStringRepn, &voxel_size) ==
	     VistaIOAttrFound) {
		if (sscanf(voxel_size, "%f %f %f", &sx, &sy, &sz) == 3) {
			mia_image3d_set_scales(result, sx, sy, sz); 
		}else{
			g_warning("unable to interpret input file voxel size attribute'%s', assume (1,1,1)", 
				  voxel_size);
		}
	}else{
		g_warning("Input file has no voxel size attribute, assume (1,1,1)");
	}


	VistaIODestroyImage(img);
	return result; 
}

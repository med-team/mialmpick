/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <string.h>
#include <gtk/gtkgl.h>
#include "callbacks.h"
#include "glview.h"
#include "landmarkinput.h"
#include "landmarktable.h"
#include "globals.h"
#include "gldrawable.h"
#include "glvolume.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <math.h>


extern MiaImage3d *load_vista_image(const char *filename);

typedef void (*file_use_function) (const gchar * filename);

typedef struct {
	GtkFileSelection *file_selection;
	file_use_function file_use;
} file_loader_data;

MiaGLVolume *volume_renderer = NULL; 
static gboolean autosnapshot_active = FALSE; 

gboolean 
do_read_volume_data (const char *filename)
{
	MiaImage3d *image = load_vista_image(filename);
	if (image) {
		if (!volume_renderer) {
			volume_renderer = mia_glvolume_new("volume_renderer", range_iso_value_min, range_iso_value_max);
			miaui_glview_add_drawable(MIAUI_GLVIEW(glview), volume_renderer);
			g_object_unref(volume_renderer);
		}
		MiaVector3d *gravity_center = mia_glvolume_set_volume(volume_renderer, image);
		if (gravity_center) 
			miaui_glview_set_rotation_center(MIAUI_GLVIEW(glview), gravity_center);
			
		miaui_glview_update_drawable(MIAUI_GLVIEW(glview), volume_renderer);
		return TRUE; 
	}
	return FALSE; 
	
}

void 
__open_file_callback(GtkButton *UNUSED(button), GtkFileSelection *fs)
{
	 
	
	const gchar *fname = gtk_file_selection_get_filename(fs);
	
	if (do_read_volume_data (fname)) {
		gchar *s = strrchr(fname, '/');
		MiauiLandmarkTable *landmark_table = 
		    MIAUI_LANDMARK_TABLE(glade_xml_get_widget (xml, 
		                         "landmark_table"));
		
		if (s) {
			++s; 
			miaui_landmark_table_set_name(landmark_table, s); 
		}else
			miaui_landmark_table_set_name(landmark_table, fname); 
		
		miaui_landmark_table_set_listname(landmark_table, NULL);
		
		gtk_widget_destroy(GTK_WIDGET(fs));
	}else
		gtk_file_selection_complete (fs, fname);
}
void 
on_landmark_table_list_change(GtkWidget *UNUSED(w), gpointer UNUSED(user_data))
{
	gtk_widget_queue_draw(glview);
}

void 
on_landmark_table_list_create(GtkWidget *UNUSED(w), MiaGLDrawable *lmd, gpointer UNUSED(user_data))
{
	miaui_glview_add_drawable(MIAUI_GLVIEW(glview), lmd);
}

void __auto_snap(MiaLandmark *lm, gchar *out_path)
{
	GString *fname = g_string_new(miaui_landmark_table_get_name(landmarktable));
	g_string_prepend(fname, out_path);
	g_string_append(fname, mia_landmark_get_name(lm));
	g_string_append(fname, ".png");
	miaui_glview_snapshot(MIAUI_GLVIEW(glview), fname->str);
	mia_landmark_set_picfile(lm, fname->str);
	g_string_free(fname, TRUE);
}

GString *get_current_dir(void)
{
	GString *dir = g_string_new(g_get_current_dir());
	g_string_append_c(dir,'/');
	return dir;
}

void on_glview_location_picked(GtkWidget *w, MiaVector3d *loc, gpointer UNUSED(user_data))
{
	MiaLandmark *lm;
	MiauiGLView *glview = MIAUI_GLVIEW(w);
	lm = miaui_landmark_table_set_landmark(landmarktable, loc, 
				  miaui_glview_get_camera(glview), 
                                  iso_value_min);

	if (autosnapshot_active && lm) {
		GString *dir = get_current_dir();
		__auto_snap(lm, dir->str);
		g_string_free(dir, TRUE);
	};
	
	miaui_landmark_table_auto_advance(landmarktable);
	gtk_widget_queue_draw(GTK_WIDGET(glview));
}

void on_landmark_table_selection_changed(GtkWidget *w, MiaLandmark *lm, gpointer UNUSED(user_data))
{
	MiauiLandmarkTable *lmt = MIAUI_LANDMARK_TABLE(w);
	
	
	if (miaui_landmark_table_use_camera(lmt))
		miaui_glview_set_camera(MIAUI_GLVIEW(glview), 
					mia_landmark_get_camera(lm));

	if (miaui_landmark_table_use_isovalue(lmt)) 	
		gtk_range_set_value(range_iso_value_min,
			mia_landmark_get_iso_value(lm));
	
	gtk_widget_queue_draw(glview);
}

void
on_open_clicked (GtkButton * UNUSED(button), gpointer UNUSED(user_data))
{
	GtkFileSelection *fs = GTK_FILE_SELECTION(gtk_file_selection_new(_("Open 3D data set")));
	
	gtk_file_selection_complete (fs, "*.v");
	g_signal_connect(fs->ok_button, "clicked", 
	                 G_CALLBACK(__open_file_callback), (gpointer)fs);
	g_signal_connect_swapped(fs->cancel_button,"clicked", 
				 G_CALLBACK(gtk_widget_destroy), (gpointer)fs);
	
	gtk_widget_show (GTK_WIDGET (fs));

}

gint __ask_save(void)
{
	gint result; 
	GtkWidget *dialog = 
		gtk_message_dialog_new (NULL,
                                  GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                  GTK_MESSAGE_QUESTION,
                                  GTK_BUTTONS_YES_NO,
                                  _("Landmarks not saved, save now?"));
	result = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
	return result; 
}


gboolean
__try_save(void)
{
	gboolean result = miaui_landmark_table_is_dirty(landmarktable);
	if (!result)	
		return TRUE; 
	
	switch (__ask_save()) {
		case GTK_RESPONSE_YES:
			return miaui_landmark_table_save_landmarks(landmarktable);
		case GTK_RESPONSE_NO:
			return TRUE; 
		default:
			return FALSE; 
	}	
}

void
on_exit_clicked (GtkButton *UNUSED(button), gpointer UNUSED(user_data))
{
	__try_save();
	gtk_widget_destroy(mainwindow);
	gtk_main_quit ();
}


void
on_about_clicked (GtkButton * UNUSED(button), gpointer UNUSED(user_data))
{
	on_about_activate(NULL, NULL); 
}

void
on_rb_rotate_toggled (GtkToggleButton * togglebutton, gpointer UNUSED(user_data))
{
	if (gtk_toggle_button_get_active (togglebutton))
		miaui_glview_set_mouse_action(MIAUI_GLVIEW(glview), miaui_glview_rotate);
}


void
on_rb_shift_toggled (GtkToggleButton * togglebutton, gpointer UNUSED(user_data))
{
	if (gtk_toggle_button_get_active (togglebutton))
		miaui_glview_set_mouse_action(MIAUI_GLVIEW(glview), miaui_glview_shift);
}


void
on_rb_zoom_toggled (GtkToggleButton * togglebutton, gpointer UNUSED(user_data))
{
	if (gtk_toggle_button_get_active (togglebutton))
		miaui_glview_set_mouse_action(MIAUI_GLVIEW(glview), miaui_glview_zoom);
}

void
on_vscale_iso_value_max_changed (GtkRange * range, gpointer UNUSED(user_data))
{
	iso_value_max = gtk_range_get_value(range);
	gtk_widget_queue_draw(glview);
}

void
on_vscale_iso_min_value_changed(GtkRange * range, gpointer UNUSED(user_data))
{
	iso_value_min = gtk_range_get_value(range);
	gtk_widget_queue_draw(glview);
}

static GtkWidget *autosnap_on_image;
static GtkWidget *autosnap_off_image;
static GtkWidget **autosnap_icon = NULL; 

void
on_mainwindow_realize(GtkWidget *UNUSED(w), gpointer UNUSED(user_data))
{
	GList *children; 
	MiauiLandmarkTable *landmark_table = 
			MIAUI_LANDMARK_TABLE(glade_xml_get_widget (xml, 
		                             "landmark_table"));
	
	GtkToolbar* toolbar =  GTK_TOOLBAR(glade_xml_get_widget (xml, "toolbar"));
		
	autosnap_on_image =  gtk_image_new_from_file (glade_xml_relative_file(xml,"auto_snapshot_on.png"));
	autosnap_off_image = gtk_image_new_from_file (glade_xml_relative_file(xml,"auto_snapshot.png"));

	
	children = toolbar->children; 
	while (children && ! autosnap_icon) {
		GtkLabel *label =  GTK_LABEL(((GtkToolbarChild *)children->data)->label);
		if (label)
			if (!strcmp( gtk_label_get_text(label),"autosnap")) {
				autosnap_icon = &((GtkToolbarChild *)children->data)->icon; 
				/*g_object_unref(*autosnap_icon);*/
				*autosnap_icon = autosnap_off_image;
			}
		
		children = children->next; 
	}
	
	
	if (in_filename) {
		do_read_volume_data (in_filename);
		in_filename = NULL; 
	}
	
	if (lm_filename) {
		MiaLandmarklist *lml = mia_landmarklist_new_from_file(lm_filename);
		miaui_landmark_table_set_landmarklist(landmark_table,lml);
	}
		
	if (out_filename) {
		miaui_landmark_table_set_listname(landmark_table, out_filename);
	}
	
}

gboolean
on_mainwindow_destroy_event (GtkWidget * UNUSED(widget),
			     GdkEvent * UNUSED(event), gpointer UNUSED(user_data))
{

	
	__try_save();
	gtk_main_quit ();
	return FALSE;
}


gboolean
on_mainwindow_delete_event (GtkWidget * UNUSED(widget),
			    GdkEvent * UNUSED(event), gpointer UNUSED(user_data))
{
	__try_save();	
	gtk_main_quit ();
	return FALSE;
}

void
on_snapshot_clicked (GtkButton * UNUSED(button), gpointer UNUSED(user_data))
{
	char name[1024]; 
	snprintf(name, 1024,"%s%04d.png", 
		 miaui_landmark_table_get_name(landmarktable),
		 snap_number++);
	miaui_glview_snapshot(MIAUI_GLVIEW(glview), name);
}

void 
on_button_fit_clicked(GtkButton * UNUSED(button), gpointer UNUSED(user_data))
{
#if 0	
	MiauiGLView *view = MIAUI_GLVIEW(glview);
	g_message("on_button_fit_clicked");
#endif	
}

void 
on_toggle_zoom_toggled(GtkToggleButton * togglebutton, gpointer UNUSED(user_data))
{
	if (gtk_toggle_button_get_active (togglebutton))
		miaui_glview_enable_zoomin(MIAUI_GLVIEW(glview));
	else
		miaui_glview_disable_zoomin(MIAUI_GLVIEW(glview));
}

void 
on_toggle_autosnap_toggled(GtkToggleButton * togglebutton, gpointer UNUSED(user_data))
{
	autosnapshot_active = gtk_toggle_button_get_active (togglebutton);
	
	if (autosnap_icon) {
		if (autosnapshot_active) {
			*autosnap_icon = autosnap_on_image;
		}else{
			*autosnap_icon = autosnap_off_image;
		}
	}
}

void 
on_cb_lmvis_toggled(GtkToggleButton * togglebutton, gpointer UNUSED(user_data))
{
	miaui_landmark_table_set_landmarks_visible(landmarktable, 
		gtk_toggle_button_get_active (togglebutton));
	gtk_widget_queue_draw(glview);
}

void 
on_cb_volvis_toggled(GtkToggleButton * togglebutton, gpointer UNUSED(user_data))
{
	if (MIA_IS_GLDRAWABLE(volume_renderer))
		MIA_GLDRAWABLE(volume_renderer)->is_visible = 
			gtk_toggle_button_get_active (togglebutton);
	gtk_widget_queue_draw(glview);
}

// Menu callbacks
void on_load_volume_activate(GtkMenuItem *UNUSED(item), gpointer user_data)
{
	on_open_clicked (NULL, user_data);
}

void on_exit_activate(GtkMenuItem *UNUSED(item), gpointer user_data)
{
	on_exit_clicked(NULL, user_data); 	
}

void on_load_landmarkset_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	miaui_landmark_table_load_handler(landmarktable);
}

void on_save_landmarkset_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	miaui_landmark_table_save_handler(landmarktable);
}

void on_save_landmarkset_as_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	miaui_landmark_table_save_as_handler(landmarktable);
}

void on_clear_locations_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	miaui_landmark_table_clear_locations_handler(landmarktable);
}

void __do_snapshot (gpointer UNUSED(key), gpointer value, gpointer user_data)
{
	MiaLandmark *lm = MIA_LANDMARK(value);
	MiaLandmarklist *lml = MIA_LANDMARKLIST(user_data);
	GString *dir = get_current_dir();
	
	g_assert(lm);
	
	if (!mia_landmark_get_location(lm))	
		return; 
	
	mia_landmarklist_set_selected(lml, mia_landmark_get_name(lm));
	
	miaui_glview_set_camera(MIAUI_GLVIEW(glview), 
		mia_landmark_get_camera(lm));

	gtk_range_set_value(range_iso_value_max,
			mia_landmark_get_iso_value(lm));

	__auto_snap(lm, dir->str);
		
	g_string_free(dir, TRUE);
}

void __do_create_template(const char *outpath)
{
	gchar *current_path = getcwd (NULL, 0);
	
	if (chdir (outpath)) {
		/* try to create the directory */
		if (mkdir(outpath, S_IRWXU | S_IRGRP |S_IXGRP |S_IROTH|S_IXOTH)) {
			global_show_error_message("Unable to create directory %s", outpath);
			goto fail; 
		}
		/* the directory was just created, so this shouldn't happen ... */
		if (!chdir( outpath )) {
			global_show_error_message("Directory %s was just created and already vanished.", outpath);
			goto fail; 
		}
	}
	/* now we are inside the target directory */ 
	/* Make a snapshot for each view */

	miaui_landmark_table_for_each_landmark(landmarktable, __do_snapshot);
	miaui_landmark_table_save_landmarks(landmarktable);
	
	if (!chdir(current_path))
		global_show_error_message("Can't return to working directory %s because it was removed.", current_path);
fail:
	free(current_path);
}

void on_create_template_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	// first get the output directory 
	GString *outdir = NULL;
	GtkFileSelection *filesel = GTK_FILE_SELECTION(gtk_file_selection_new(_("Select output directory"))); 
	if (gtk_dialog_run(GTK_DIALOG(filesel)) == GTK_RESPONSE_OK) {
		outdir = g_string_new(gtk_file_selection_get_filename(filesel));
	}
	gtk_widget_destroy(GTK_WIDGET(filesel));
	
	if (outdir) {
		__do_create_template(outdir->str);
		g_string_free(outdir,TRUE);
	}	
}

void on_add_landmark_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	miaui_landmark_table_add_landmark_handler(landmarktable);	
}

void on_edit_landmark_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	miaui_landmark_table_edit_landmark_handler(landmarktable);	
}

void on_clear_landmark_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	miaui_landmark_table_clear_landmark_handler(landmarktable);	
}

void on_delete_landmark_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	miaui_landmark_table_del_landmark_handler(landmarktable);	
}

void on_about_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	gchar *authors[] = {"Gert Wollny <gw.fossdev@gmail.com>", NULL}; 

	gtk_show_about_dialog(NULL, 
			      "program-name", _("MIA landmark picker"), 
			      "version", VERSION, 
			      "comments", _("This software is a tool to view iso-surfaces in volume data\n"
					    "and pick landmarks\n\n"), 
			      "website", "http://sourceforge.net/p/mia/wiki/lmpick/",
			      "authors", authors, 
			      "copyright",   
			      _("This software is Copyright (c) 1999-2013 Leipzig, Germany and Madrid, Spain.\n"
				"It comes with ABSOLUTELY NO WARRANTY and you may redistribute it under the\n"
				" terms of the GNU GENERAL PUBLIC LICENSE Version 3 (or later)."), 
			      NULL
		) ; 

}

static void do_set_view(float zoom, float x, float y, float z, float r)
{
	MiaCamera *camera = miaui_glview_get_camera (MIAUI_GLVIEW(glview));
	
	MiaVector3d *axis = mia_vector3d_new(x,y,z);
	MiaQuaternion *rot = mia_quaternion_new_from_eulerrot(axis, r);
	mia_camera_set_rotation(camera, rot);
	mia_camera_set_zoom(camera, zoom);
	g_object_unref(axis);
	
	miaui_glview_reset_camera (MIAUI_GLVIEW(glview));
}	

void on_frontal_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	//do_set_view(1.0, 1.0, 0.0, 0.0, -M_PI / 2.0);
	do_set_view(1.0, 0, -1.0, 1.0, M_PI );
}

void on_back_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	do_set_view(1.0, 1.0, 0.0, 0.0, -M_PI / 2.0);
}	

void on_top_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	do_set_view(1.0, 1.0, 0.0, 0.0, M_PI);
}	

void on_bottom_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	do_set_view(1.0, 0.0, 0.0, 1.0, M_PI);
}	

void on_left_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	do_set_view(1.0,-1.0, -1.0, 1.0, M_PI * 2.0 / 3.0);
}	

void on_right_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	do_set_view(1.0, 1.0, -1.0, 1.0, -M_PI * 2.0 / 3.0);
}

void on_background_activate(GtkMenuItem *UNUSED(item), gpointer UNUSED(user_data))
{
	gdouble *color_ptr = miaui_glview_get_back_color_ptr(MIAUI_GLVIEW(glview));
	GtkColorSelectionDialog *amy = (GtkColorSelectionDialog *)gtk_color_selection_dialog_new(_("Select a background colour"));
	
	gdouble old_color[4];
	memcpy(old_color, color_ptr, 4 * sizeof(gdouble));
	
	gtk_color_selection_set_color((GtkColorSelection *)amy->colorsel, color_ptr);
	gint result = gtk_dialog_run (GTK_DIALOG (amy));
	switch (result)  {	
	      case GTK_RESPONSE_OK:	
		      gtk_color_selection_get_color((GtkColorSelection *)amy->colorsel, color_ptr);
		      break;
	      default:
		      memcpy(color_ptr, old_color, 4 * sizeof(gdouble));
		      break;
	}
	gtk_widget_destroy(GTK_WIDGET(amy));
}

/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

 
#ifndef __image3d_h
#define __image3d_h

#include <glib.h>
#include <glib-object.h>
	

#define MIA_TYPE_IMAGE3D (mia_image3d_get_type())
#define MIA_IMAGE3D(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIA_TYPE_IMAGE3D, MiaImage3d))
#define MIA_IMAGE3D_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIA_TYPE_IMAGE3D, MiaImage3dClass))
#define MIA_IS_IMAGE3D(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIA_TYPE_IMAGE3D))
#define MIA_IS_IMAGE3D_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIA_TYPE_IMAGE3D))
#define MIA_IMAGE3D_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst), MIA_TYPE_IMAGE3D, MiaImage3dClass))


typedef struct _MiaImage3d MiaImage3d;
typedef struct _MiaImage3dClass MiaImage3dClass;

struct _MiaImage3d {
	GObject parent; 
	guint nx,ny,nz; 
	guint size;
	gboolean minmax_is_valid ; 
	gfloat min, max; 
	gfloat sx, sy, sz; 
	gfloat *data;
};

struct _MiaImage3dClass {
	GObjectClass parent;
};

GType mia_image3d_get_type(void);

MiaImage3d *mia_image3d_new_ubyte(gint nx, gint ny, gint nz, guint8 *data);

MiaImage3d *mia_image3d_new_short(gint nx, gint ny, gint nz, gint16 *data);

void mia_image3d_set_scales(MiaImage3d *self, gfloat sx, gfloat sy, gfloat sz);

void mia_image3d_get_minmax(MiaImage3d *self, gfloat *min, gfloat *max);

gboolean mia_image3d_test(void);

MiaImage3d *mia_image3d_crop_pow2(MiaImage3d *src, gint max_n);

#endif

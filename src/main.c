/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <X11/Xlib.h>
#include <popt.h>
#include <glade/glade.h>
#include "globals.h"
#include "datapath.h"
#include <gtk/gtkgl.h>



#ifndef NDEBUG
extern void print_lml_left(void);
extern void print_camera_left(void);
extern void print_quats_left(void);
extern void print_vectors_left(void);
#endif


void MIAGLogFunc(const gchar *log_domain,
		 GLogLevelFlags log_level,
		 const gchar *message,
		 gpointer UNUSED(user_data))
{
	if (log_level & (G_LOG_FLAG_RECURSION |
			 G_LOG_FLAG_FATAL|
			 G_LOG_LEVEL_ERROR|
			 G_LOG_LEVEL_CRITICAL|
			 G_LOG_LEVEL_WARNING| G_LOG_LEVEL_MESSAGE)) 
		printf("%s:%s\n", log_domain, message); 
}
		

int main (int argc, char *argv[])
{
//	GtkWidget *landmark_table; 
	gint major, minor;

	int rc;
	int help = 0;
	int usage = 0;
	poptContext optCon;
	char *glade_filename = NULL; 
		
	struct poptOption options[] = {	
		{ "landmarkset",  'i', POPT_ARG_STRING, &lm_filename, 0,
		  "initial landmark set", "landmarkset" },
		{ "outputset",  'o', POPT_ARG_STRING, &out_filename, 0,
		  "output landmark set", "outputset" },
		{ "volume",  'v', POPT_ARG_STRING, &in_filename, 0,
		  "volume image (only unsigned short and unsigned byte are supported)", "volume" },
		{ "glade-file",  0, POPT_ARG_STRING, &glade_filename, 0,
		  "use specified glade interface file (only useful when run from source tree)", "glade" },
		POPT_AUTOHELP
                { NULL, 0, 0, NULL, 0, 0, 0 }
	};

#if GLIB_MAJOR_VERSION == 2 && GLIB_MINOR_VERSION < 36
	g_type_init();
#endif 
/*	
	g_thread_init(NULL);
*/
	XInitThreads(); 
	gdk_threads_init();	

	g_log_set_default_handler(MIAGLogFunc, NULL);

#ifdef ENABLE_NLS
	bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (PACKAGE);
#endif

#if 0
	gnome_init_with_popt_table(PACKAGE, VERSION, argc, argv, options,0, &optCon);
	glade_gnome_init ();
	gtk_gl_init(&argc, &argv);

#else
	gtk_init(&argc, &argv);
	gdk_gl_init(&argc, &argv);
	const char **args = (const char **)argv; 
	optCon	= poptGetContext(args[0], argc, args, options, 0);
	glade_init();
#endif
	gdk_gl_query_version (&major, &minor);
	g_message("Print version"); 
	g_print ("\nOpenGL extension version - %d.%d\n",
		 major, minor);
	

	/*
	 * The .glade filename should be on the next line.
	 */
	
	if ((rc = poptGetNextOpt(optCon)) < -1) {
		fprintf(stderr, "%s: bad argument %s: %s\n", argv[0],
			poptBadOption(optCon, POPT_BADOPTION_NOALIAS), 
			poptStrerror(rc));
		poptPrintUsage(optCon, stdout, 0);
		return 2;
	}
	if (help) {
		poptPrintHelp(optCon, stdout, 0);
		return 0;
	} 
	if (usage) {
		poptPrintUsage(optCon, stdout, 0);
		return 0;
	}

	g_message("load glade file ... "); 
			
	if ( glade_filename  )
		xml = glade_xml_new (glade_filename, NULL, NULL);
	else 
		xml = glade_xml_new (PACKAGE_DATA_DIR "/mia-lmpick.glade", NULL, NULL);

	g_message("done"); 
	if (xml)  {
		

		glade_xml_signal_autoconnect (xml);
		mainwindow = glade_xml_get_widget (xml, "mainwindow");
		glview = glade_xml_get_widget (xml, "da_volrend");
		range_iso_value_max = GTK_RANGE(glade_xml_get_widget (xml, "vscale_iso_max"));
		range_iso_value_min = GTK_RANGE(glade_xml_get_widget (xml, "vscale_iso_min"));
		//	landmark_table = glade_xml_get_widget(xml, "landmark_table");
		gtk_widget_show (mainwindow);
		
		gdk_threads_enter();
		gtk_main ();
		gdk_threads_leave();
	}else {
		g_error("vlmpick.glade not found"); 
	}
#ifndef NDEBUG
	print_lml_left();
	print_camera_left();
	print_quats_left();
	print_vectors_left();
#endif

	return 0;
}

/* -*- mode:C; indent-tabs-mode: nil; tab-width: 8; c-basic-offset: 8; c-set-style "K&R"; -*- */

/* 
 * This file is part of MIALMPICK - a software for landmark picking 
 * in 3D volume data sets
 *
 * Copyright (c) Leipzig, Madrid 1999-2013 Gert Wollny
 *
 * MIALMPICK is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MIALMPICK; if not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#ifndef __landmarkinput_h
#define __landmarkinput_h

#include <gtk/gtk.h>
#include "landmarktable.h"


#define MIAUI_TYPE_LANDMARK_INPUT (miaui_landmark_input_get_type())
#define MIAUI_LANDMARK_INPUT(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), MIAUI_TYPE_LANDMARK_INPUT, MiauiLandmarkInput))
#define MIAUI_LANDMARK_INPUT_CLASS(vtable) (G_TYPE_CHECK_CLASS_CAST((vtable),MIAUI_TYPE_LANDMARK_INPUT, MiauiLandmarkInputClass))
#define MIAUI_IS_LANDMARK_INPUT(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),MIAUI_TYPE_LANDMARK_INPUT))
#define MIAUI_IS_LANDMARK_INPUT_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((obj),MIAUI_TYPE_LANDMARK_INPUT))
#define MIAUI_LANDMARK_INPUT_GET_CLASS(inst) (G_TYPE_INSTANCE_GET_CLASS((inst),MIAUI_TYPE_LANDMARK_INPUT,  MiauiLandmarkInputClass))

typedef struct _MiauiLandmarkInput MiauiLandmarkInput;
typedef struct _MiauiLandmarkInputClass MiauiLandmarkInputClass;

struct _MiauiLandmarkInputClass {
	GtkWindowClass parent;
};

typedef void (*miaui_landmarkinput_callback)(const gchar *newname, MiaLandmark *lm, 
					     MiauiLandmarkTable *user_data, GtkWidget *view);

void miaui_landmark_input_set_add_callback(MiauiLandmarkInput *self, 
					   miaui_landmarkinput_callback callback, 
					   MiauiLandmarkTable *user_data, GtkWidget *glview);

GType miaui_landmark_input_get_type(void);

MiauiLandmarkInput *miaui_landmark_input_new(gchar * titel, MiaLandmark *lm);


gboolean miaui_landmark_input_test(void);

#endif
